// @license https://www.gnu.org/licenses/gpl-3.0.html GPL-3.0-or-later
import { langTitles } from "./lang_titles.js";
function stripIds(elem) {
    for (const c of elem.children) {
        c.removeAttribute("id");
        stripIds(c);
    }
}
export function fnPreviews(registerPlayback) {
    const A_CODE_PT = 0x61;
    const Z_CODE_PT = 0x7a;
    const mainNarrow = document.getElementById("main-narrow");
    document.querySelectorAll('a[href^="#fn-"]').forEach(a => {
        if (!(a instanceof HTMLElement)) {
            return;
        }
        const fnId = a.getAttribute("href")?.slice(1);
        if (fnId === undefined) {
            console.error("fnId = undefined. a = ", a);
            return;
        }
        const fnIdSplit = fnId.split("-");
        const fnNumStr = fnIdSplit[fnIdSplit.length - 1];
        if (fnNumStr === undefined) {
            console.error("fnNumStr = undefined. a = ", a);
            return;
        }
        const fnNum = parseInt(fnNumStr, 10);
        if (!Number.isFinite(fnNum)) {
            const codePt = fnNumStr.codePointAt(0);
            if (codePt === undefined ||
                fnNumStr.length !== 1 ||
                !(codePt >= A_CODE_PT && codePt <= Z_CODE_PT)) {
                console.error(`fnNumStr = "${fnNumStr}". a = `, a);
            }
            return;
        }
        const backArrowParent = document.getElementById(fnId)?.parentElement;
        if (!backArrowParent) {
            console.error(`No parent element. fnId = "${fnId}". a = `, a);
            return;
        }
        const li = (() => {
            switch (backArrowParent.tagName) {
                case "P":
                    return backArrowParent.parentElement;
                case "LI":
                    return backArrowParent;
                default: {
                    console.warn("backArrowParent is neither a <p> nor a <li>. " +
                        "backArrowParent = ", backArrowParent);
                    return backArrowParent;
                }
            }
        })();
        if (!li) {
            console.error("Back arrow has no grandparent. a = ", a);
            return;
        }
        const cx = (() => {
            if (a.parentElement && a.parentElement.parentElement) {
                const fnmParent = a.parentElement.parentElement;
                if (fnmParent.classList.contains("fnm-cx")) {
                    return fnmParent;
                }
            }
            return;
        })();
        const liClone = li.cloneNode(true);
        const fnStart = liClone.children[0];
        switch (fnStart?.tagName) {
            case "A": {
                liClone.removeChild(fnStart);
                break;
            }
            case "P": {
                const pStart = fnStart.children[0];
                if (pStart?.tagName === "A") {
                    fnStart.removeChild(pStart);
                }
                else {
                    console.warn("First child of ", fnStart, " is not an <a>.");
                }
                break;
            }
            default: {
                console.warn("First child of ", liClone, " is neither an <a> nor a <p>.");
                break;
            }
        }
        stripIds(liClone);
        liClone.querySelectorAll('a[href$=".opus"]').forEach(registerPlayback);
        langTitles(liClone);
        const aside = document.createElement("aside");
        aside.classList.add("fn-preview");
        const newOl = document.createElement("ol");
        newOl.setAttribute("start", "" + fnNum);
        newOl.append(liClone);
        aside.append(newOl);
        let previewed = false;
        let closePreviewTimeout = undefined;
        const openPreview = () => {
            if (previewed) {
                clearTimeout(closePreviewTimeout);
                return;
            }
            previewed = true;
            document.body.append(aside);
            cx?.classList.add("cx-highlight");
            const aRect = a.getBoundingClientRect();
            const mainNarrowRect = mainNarrow.getBoundingClientRect();
            const initTop = Math.ceil(aRect.top +
                window.scrollY -
                document.documentElement.clientTop);
            aside.style.top = initTop + "px";
            const initLeft = Math.ceil(mainNarrowRect.left +
                window.scrollX -
                document.documentElement.clientLeft);
            aside.style.left = initLeft + "px";
            aside.style.maxWidth = Math.floor(mainNarrowRect.width) + "px";
            const below = aRect.top <= aside.clientHeight + 1;
            const top = Math.floor(below
                ? aRect.bottom +
                    window.scrollY -
                    document.documentElement.clientTop
                : aRect.top +
                    window.scrollY -
                    document.documentElement.clientTop -
                    aside.clientHeight);
            aside.style.top = top + "px";
            const asideRect = aside.getBoundingClientRect();
            const asideMid = asideRect.x + asideRect.width / 2.0;
            const aMid = aRect.x + aRect.width / 2.0;
            const left = Math.floor(initLeft +
                Math.min(Math.max(aMid - asideMid, 0), mainNarrowRect.width - aside.clientWidth));
            aside.style.left = left + "px";
        };
        const closePreview = () => {
            if (!previewed) {
                return;
            }
            clearTimeout(closePreviewTimeout);
            closePreviewTimeout = setTimeout(() => {
                if (!previewed) {
                    return;
                }
                previewed = false;
                document.body.removeChild(aside);
                cx?.classList.remove("cx-highlight");
            }, 200);
        };
        a.addEventListener("mouseenter", openPreview);
        a.addEventListener("mouseleave", closePreview);
        a.addEventListener("focus", openPreview);
        a.addEventListener("blur", closePreview);
        a.addEventListener("click", closePreview);
        aside.addEventListener("mouseenter", openPreview);
        aside.addEventListener("mouseleave", closePreview);
    });
}
// @license-end
