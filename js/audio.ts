// @license https://www.gnu.org/licenses/gpl-3.0.html GPL-3.0-or-later

/**
 * Registers click-to-playback functionality for anything in the document that
 * matches `a[href$=".opus"]`.
 *
 * @returns A callback that can be used to register more elements.
 */
export function opusSetup(): (a: Element) => void {
    let nowPlaying: HTMLAudioElement | undefined = undefined;
    function registerPlayback(a: Element): void {
        a.setAttribute("target", "_blank");
        a.classList.add("instant-playback");

        let audio: HTMLAudioElement | undefined = undefined;
        const play = () => {
            if (!audio) {
                return;
            }
            if (nowPlaying !== undefined) {
                nowPlaying.pause();
                nowPlaying.currentTime = 0;
            }
            if (audio.paused) {
                audio
                    .play()
                    .catch(err =>
                        console.error("Could not play audio:\n ", err),
                    );
            } else {
                audio.currentTime = 0;
            }
            nowPlaying = audio;
        };
        const activate = () => {
            if (audio === undefined) {
                audio = document.createElement("audio");
                audio.setAttribute("preload", "auto");
                const src = a.getAttribute("href");
                if (!src) {
                    console.error("href attribute of ", a, " is null");
                    return;
                }
                audio.setAttribute("src", src);
            }

            play();
        };
        a.addEventListener("click", ev => {
            activate();
            ev.stopPropagation();
            ev.preventDefault();
        });
        a.addEventListener("keydown", ev => {
            if (!(ev instanceof KeyboardEvent) || ev.key !== "Enter") {
                return;
            }
            activate();
            ev.stopPropagation();
            ev.preventDefault();
        });
    }

    document.querySelectorAll('a[href$=".opus"]').forEach(registerPlayback);

    return registerPlayback;
}

// @license-end
