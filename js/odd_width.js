// @license https://www.gnu.org/licenses/gpl-3.0.html GPL-3.0-or-later
const DEBOUNCE_INTERVAL = 750;
const ONLY_NODE_NAMES = new Set(["IMG", "VIDEO", "AUDIO"]);
/**
 * Shitty hack to deal with Chromium-based and WebKit-based browsers rendering
 * images at non-integer pixel positions, making them look blurry. 🙄
 *
 * See also: “half-pel filter”, “quarter-pel”, “fractional pixels”, etc.,
 * <https://stackoverflow.com/questions/39861687/how-to-prevent-fractional-pixels-in-an-element-with-width-set-to-auto>
 */
function setOddWidthClass(elem) {
    if (elem.clientWidth % 2 === 1 ||
        (elem.clientWidth === 0 &&
            elem.nodeName === "IMG" &&
            elem.naturalWidth % 2 === 1)) {
        elem.classList.add("odd-width");
    }
    else {
        elem.classList.remove("odd-width");
    }
}
function updateOddWidths() {
    document
        .querySelectorAll("#main-narrow img, " +
        '#main-narrow video, #main-narrow math[display="block"]')
        .forEach(setOddWidthClass);
}
function removeFigOutlines() {
    document.querySelectorAll("figure").forEach(fig => {
        let witnessedChild = false;
        for (const child of fig.children) {
            if (child.nodeName === "FIGCAPTION") {
                continue;
            }
            if (witnessedChild) {
                return;
            }
            witnessedChild = true;
            if (ONLY_NODE_NAMES.has(child.nodeName) ||
                (child.nodeName === "P" &&
                    child.children.length === 1 &&
                    child.children[0] &&
                    ONLY_NODE_NAMES.has(child.children[0].nodeName))) {
                continue;
            }
            return;
        }
        fig.classList.add("no-outline");
    });
}
export function oddWidths() {
    updateOddWidths();
    removeFigOutlines();
    let debouncedUpdate = undefined;
    window.addEventListener("resize", () => {
        clearTimeout(debouncedUpdate);
        debouncedUpdate = setTimeout(updateOddWidths, DEBOUNCE_INTERVAL);
    });
    document
        .querySelectorAll("#main-narrow img")
        .forEach(img => img.addEventListener("load", () => setOddWidthClass(img)));
    document.querySelectorAll("details").forEach(details => details.addEventListener("toggle", () => {
        if (details.open) {
            updateOddWidths();
        }
    }));
}
// @license-end
