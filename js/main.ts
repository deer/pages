// @license https://www.gnu.org/licenses/gpl-3.0.html GPL-3.0-or-later

import { crumbs } from "./crumbs.js";
import { fnPreviews } from "./fn_preview.js";
import { documentLangTitles } from "./lang_titles.js";
import { oddWidths } from "./odd_width.js";
import { opusSetup } from "./audio.js";

window.addEventListener("load", () => {
    oddWidths();
    const registerPlayback = opusSetup();
    if (!window.requestIdleCallback) {
        setTimeout(() => {
            crumbs();
            setTimeout(() => {
                fnPreviews(registerPlayback);
                setTimeout(documentLangTitles, 1);
            }, 1);
        }, 1);
    } else {
        window.requestIdleCallback(() => {
            crumbs();
            window.requestIdleCallback(() => {
                fnPreviews(registerPlayback);
                window.requestIdleCallback(documentLangTitles);
            });
        });
    }
});

// @license-end
