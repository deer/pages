// @license https://www.gnu.org/licenses/gpl-3.0.html GPL-3.0-or-later
import { langTitles } from "./lang_titles.js";
const DEBOUNCE_INTERVAL = 250;
export function crumbs() {
    const h1Text = document.querySelector("h1")?.innerText;
    if (!h1Text ||
        !(h1Text.includes("diary:") || h1Text.includes("The adventures of"))) {
        return;
    }
    const headings = [];
    const idToHeading = new Map();
    const crumbsNav = document.createElement("nav");
    let crumbsOl = document.createElement("ol");
    crumbsNav.append(crumbsOl);
    crumbsNav.classList.add("crumbs");
    document.body.append(crumbsNav);
    function updateCrumbs(entries) {
        const topEntry = entries.reduce((best, curr) => curr.boundingClientRect.y > best.boundingClientRect.y &&
            curr.isIntersecting
            ? curr
            : best);
        let headingIx = idToHeading.get(topEntry.target.id);
        if (headingIx === undefined) {
            console.error("headingIx = undefined");
            return;
        }
        if (!topEntry.isIntersecting &&
            topEntry.boundingClientRect.y >
                document.documentElement.clientHeight / 4.0) {
            headingIx = Math.max(0, headingIx - 1);
        }
        crumbsOl.remove();
        crumbsOl = document.createElement("ol");
        let heading;
        while (headingIx !== undefined && (heading = headings[headingIx])) {
            const crumbLi = document.createElement("li");
            const crumbA = document.createElement("a");
            crumbA.innerHTML = heading.name;
            crumbA.href = "#" + heading.id;
            crumbLi.append(crumbA);
            crumbLi.classList.add("crumb-h" + heading.level);
            crumbsOl.prepend(crumbLi);
            headingIx = heading.parent;
        }
        crumbsNav.append(crumbsOl);
        langTitles(crumbsNav);
    }
    let debouncedUpdate = undefined;
    function updateCrumbsDebounced(entries) {
        clearTimeout(debouncedUpdate);
        debouncedUpdate = setTimeout(() => updateCrumbs(entries), DEBOUNCE_INTERVAL);
    }
    const stack = [];
    const iso = new IntersectionObserver(updateCrumbsDebounced, {
        root: null,
        rootMargin: "0% 0% -50% 0%",
        threshold: 1.0,
    });
    document.querySelectorAll("h1, h2, h3, h4, h5, h6").forEach(elem => {
        const level = (() => {
            switch (elem.nodeName) {
                case "H1":
                    return 1;
                case "H2":
                    return 2;
                case "H3":
                    return 3;
                case "H4":
                    return 4;
                case "H5":
                    return 5;
                case "H6":
                    return 6;
            }
            console.error(elem, " is not a heading.");
            return 7;
        })();
        if (level > 6) {
            return;
        }
        const parent = stack[level - 2];
        stack.length = level;
        stack[level - 1] = headings.length;
        const id = elem.id;
        idToHeading.set(id, headings.length);
        headings.push({
            level,
            name: elem.firstElementChild?.nodeName === "A"
                ? elem.firstElementChild.innerHTML
                : elem.innerHTML,
            id,
            parent,
        });
        iso.observe(elem);
    });
}
// @license-end
