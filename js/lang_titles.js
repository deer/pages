// @license https://www.gnu.org/licenses/gpl-3.0.html GPL-3.0-or-later
/**
 * **NOTE:** If you’re thinking of re-using this code, probably **don’t**.
 * This is entirely bespoke, written only for my own incredibly specific
 * use-case, & does **not** make any attempt to parse IETF BCP 47 tags in
 * general.
 */
/** Primary language subtags. */
const PRIMARIES = new Map([
    ["ae", "Avestan"],
    ["ang", "Old\u00a0English"],
    ["apc", "Levantine\u00a0Arabic"],
    ["ar", "Arabic"],
    ["arb", "Modern\u00a0Standard\u00a0Arabic"],
    ["arz", "Egyptian\u00a0Arabic"],
    ["az", "Azeri"],
    ["azb", "South\u00a0Azeri"],
    ["azj", "North\u00a0Azeri"],
    ["bg", "Bulgarian"],
    ["cdo", "Eastern\u00a0Min"],
    ["cel", "Proto-Celtic"],
    ["cmn", "Mandarin"],
    ["cy", "Welsh"],
    ["de", "High\u00a0German"],
    ["dnt", "(Mid Grand Valley) Dani"],
    ["egy", "Ancient\u00a0Egyptian"],
    ["el", "Modern\u00a0Greek"],
    ["emy", "Classic\u00a0Maya"],
    ["en", "English"],
    ["enm", "Middle\u00a0English"],
    ["es", "Spanish"],
    ["fa", "Persian"],
    ["fr", "French"],
    ["frc", "Louisiana\u00a0French"],
    ["frk", "Frankish"],
    ["fro", "Old\u00a0French"],
    ["ga", "Irish"],
    ["gd", "Scottish\u00a0Gaelic"],
    ["gem", "Proto-Germanic"],
    ["gmw", "Proto-West-Germanic"],
    ["goh", "Old\u00a0High\u00a0German"],
    ["got", "Gothic"],
    ["grc", "Ancient\u00a0Greek"],
    ["grk", "Hellenic"],
    ["hbo", "Ancient\u00a0Hebrew"],
    ["he", "Modern\u00a0Hebrew"],
    ["hi", "Hindi"],
    ["hy", "(Eastern) Armenian"],
    ["iir", "Proto-Indo-Iranian"],
    ["ine", "Proto-Indo-European"],
    ["ira", "Proto-Iranian"],
    ["is", "Icelandic"],
    ["it", "Italian"],
    ["itc", "Proto-Italic"],
    ["ja", "Japanese"],
    ["jam", "Jamaican\u00a0Creole"],
    ["ko", "Korean"],
    ["la", "Latin"],
    ["lkt", "Lakota"],
    ["ltc", "Middle\u00a0Sinitic"],
    ["mga", "Middle\u00a0Irish"],
    ["ms", "Malay(ic)"],
    ["nah", "N\u0101huatl"],
    ["nan", "Southern\u00a0Min"],
    ["nci", "Classical\u00a0N\u0101huatl"],
    ["nl", "Dutch"],
    ["non", "Old\u00a0Norse"],
    ["och", "Old\u00a0Sinitic"],
    ["pal", "Middle\u00a0Persian (Pahlavi)"],
    ["peo", "Old\u00a0Persian"],
    ["pes", "Western\u00a0Persian"],
    ["pl", "Polish"],
    ["prs", "Dari"],
    ["pt", "Portuguese"],
    ["qya", "Quenya"],
    ["ru", "Russian"],
    ["sa", "Sanskrit"],
    ["sem", "Proto-Semitic"],
    ["sga", "Old\u00a0Irish"],
    ["sla", "Proto-Slavic"],
    ["tg", "Tajik"],
    ["tr", "Turkish"],
    ["trk", "Turkic"],
    ["und", "[unspecified]"],
    ["ur", "Urdu"],
    ["vi", "Vietnamese"],
    ["wuu", "Wu"],
    ["xlo", "Loup\u00a0A"],
    ["xmn", "Middle\u00a0Persian (Manichaean)"],
    ["xno", "Anglo-Norman"],
    ["xpg", "Phrygian"],
    ["xpi", "Pictish"],
    ["xpr", "Parthian"],
    ["xtg", "Gaulish (Transalpine)"],
    ["yol", "Yola"],
    ["zh", "Sinitic"],
    ["zle", "East\u00a0Slavic"],
    ["zsm", "Standard\u00a0Malaysian"],
]);
/** Script subtags. */
const SCRIPTS = new Map([
    ["arab", "(Perso-)Arabic"],
    ["aran", "Nasta\u02bel\u012bq ((Perso-)Arabic)"],
    ["bopo", "Bopomofo (Zh\u00f9y\u012bn)"],
    ["cyrl", "Cyrillic"],
    ["egyp", "Egyptian\u00a0hieroglyphs"],
    ["grek", "Greek"],
    ["hani", "Han characters"],
    ["hans", "simplified\u00a0Sinographs"],
    ["hant", "traditional\u00a0Sinographs"],
    ["hira", "Hiragana"],
    ["jamo", "Hangeul\u00a0jamo"],
    ["latf", "blackletter (Latin)"],
    ["latn", "Latin"],
    ["lyci", "Lycian\u2013Phrygian"],
    ["ogam", "Ogham"],
    ["phli", "Inscriptional\u00a0Pahlavi"],
    ["prti", "Inscriptional\u00a0Parthian"],
    ["runr", "a Runic script"],
    ["xpeo", "Old\u00a0Persian\u00a0cuneiform"],
]);
/** Region subtags. */
const REGIONS = new Map([
    ["021", "Northern\u00a0America"],
    ["au", "Australia"],
    ["br", "Brazil"],
    ["ca", "Canada"],
    ["cn", "Mainland China (\u201cB\u011bij\u012bng\u201d)"],
    ["gb", "the United\u00a0Kingdom"],
    ["ie", "Ireland"],
    ["tw", "Taiwan"],
    ["us", "the United\u00a0States"],
]);
/** Variant subtags. */
const VARIANTS = new Map([
    ["alalc97", "ALA\u2013LC-style Romanisation"],
    ["emodeng", "Early\u00a0Modern\u00a0English"],
    ["fonipa", "International\u00a0Phonetic\u00a0Alphabet"],
    ["hepburn", "Hepburn Romanisation"],
    ["itihasa", "Epic\u00a0Sanskrit"],
    ["pehoeji", "Pe\u030dh-\u014de-j\u012b Romanisation"],
    ["pinyin", "H\u00e0ny\u01d4\u00a0P\u012bny\u012bn"],
    ["vaidika", "Vedic\u00a0Sanskrit"],
]);
/**
 * Script subtags whose names shouldn’t be preceeded by "the" & followed by
 * "script" (because they’re already non-singular nouns).
 */
const NOUN_SCRIPTS = new Set(["egyp", "hani", "hans", "hant", "jamo", "runr"]);
/** Variant subtags that are languages in their own right. */
const VARIANT_LANGS = new Set(["emodeng", "itihasa", "vaidika"]);
/** Languages that do use, may use, or have historically used Jawi. */
const JAWI_LANGS = new Set([
    "ace",
    "bjn",
    "btj",
    "bve",
    "bvu",
    "coa",
    "dup",
    "hji",
    "id",
    "jak",
    "jax",
    "kvb",
    "kvr",
    "kxd",
    "lce",
    "lcf",
    "liw",
    "max",
    "mdh",
    "meo",
    "mfa",
    "mfb",
    "min",
    "mqg",
    "mrw",
    "ms",
    "msi",
    "mui",
    "orn",
    "ors",
    "pel",
    "pse",
    "tft",
    "tmw",
    "tsg",
    "urk",
    "vkk",
    "vkt",
    "xmm",
    "zlm",
    "zmi",
    "zsm",
]);
/** Languages that do use, may use, or have historically used Pegon. */
const PEGON_LANGS = new Set(["bac", "id", "jas", "jv", "jvn", "mad", "su"]);
const SCRIPT_RE = /^[a-z]{4}$/;
const REGION_RE = /^([a-z]{2}|[0-9]{3})$/;
const VARIANT_RE = /^([a-z0-9]{5,8}|[0-9][a-z0-9]{3})$/;
function giveTitle(e) {
    if (!(e instanceof HTMLElement)) {
        return;
    }
    const subtags = e.lang.toLowerCase().split("-");
    const lang_tag = subtags[0];
    if (lang_tag === undefined) {
        console.error(e, " has an empty `lang` attribute.");
        return;
    }
    let primary = PRIMARIES.get(lang_tag);
    if (primary === undefined) {
        console.warn(e, " has a `lang` attribute with no recognised primary language " +
            "subtag.");
        return;
    }
    if (primary === "English" && subtags.length <= 1) {
        return;
    }
    let script = undefined;
    let region = undefined;
    let variants = [];
    let ipa = false;
    let noun_script = false;
    for (let i = 1; i < subtags.length; ++i) {
        const subtag = subtags[i];
        if (subtag === undefined) {
            continue;
        }
        if (SCRIPT_RE.test(subtag)) {
            if (script !== undefined) {
                console.warn(`More than one script subtag: ${subtags}`);
                return;
            }
            script = SCRIPTS.get(subtag);
            if (script === undefined) {
                console.warn(`Unrecognised script subtag: ${subtag}`);
                return;
            }
            noun_script = NOUN_SCRIPTS.has(subtag);
            if (subtag === "hani") {
                switch (lang_tag) {
                    case "ja":
                    case "jpx":
                    case "ojp":
                        script = "Kanji";
                        break;
                    case "ko":
                    case "okm":
                    case "oko":
                        script = "Hanja";
                        break;
                    case "vi":
                        script = "Ch\u1eef\u00a0N\u00f4m";
                        break;
                    case "za":
                    case "zch":
                    case "zeh":
                    case "zgb":
                    case "zgm":
                    case "zgn":
                    case "zhd":
                    case "zhn":
                    case "zlj":
                    case "zln":
                    case "zlq":
                    case "zqe":
                    case "zyb":
                    case "zyg":
                    case "zyj":
                    case "zyn":
                    case "zzj":
                    case "nut":
                        script = "Sawndip";
                        break;
                }
            }
            if (subtag === "arab" || subtag === "aran") {
                if (PEGON_LANGS.has(lang_tag)) {
                    script = "Pegon";
                }
                if (JAWI_LANGS.has(lang_tag)) {
                    script = "Jawi";
                }
            }
        }
        else if (REGION_RE.test(subtag)) {
            if (region !== undefined) {
                console.warn(`More than one region subtag: ${subtags}`);
                return;
            }
            region = REGIONS.get(subtag);
            if (region === undefined) {
                console.warn(`Unrecognised region subtag: ${subtag}`);
                return;
            }
        }
        else if (VARIANT_RE.test(subtag)) {
            const variant = VARIANTS.get(subtag);
            if (variant === undefined) {
                console.warn(`Unrecognised variant subtag: ${subtag}`);
                return;
            }
            if (subtag === "fonipa") {
                ipa = true;
            }
            else if (VARIANT_LANGS.has(subtag)) {
                primary = variant;
            }
            else {
                variants.push(variant);
            }
        }
        else {
            console.warn(`Unrecognised subtag: ${subtag}`);
            return;
        }
    }
    if (ipa) {
        if (script) {
            console.warn(`Script specified for IPA: ${subtags}`);
        }
        if (lang_tag === "und") {
            e.setAttribute("title", "Transcription in the International Phonetic Alphabet (IPA)");
            return;
        }
    }
    let title = ipa
        ? `Transcription in the International Phonetic Alphabet (IPA): ${primary}-language speech`
        : `${primary}-language text`;
    if (region) {
        title += `, as spoken in ${region}`;
    }
    if (script) {
        title += noun_script
            ? `, written in ${script}`
            : `, written in the ${script} script`;
    }
    let firstVar = true;
    for (const variant of variants) {
        title += firstVar ? "; " : ", ";
        title += variant;
        firstVar = false;
    }
    e.setAttribute("title", title);
}
export function langTitles(elem) {
    elem.querySelectorAll('[lang]:not([title], [lang=""])').forEach(giveTitle);
}
export function documentLangTitles() {
    document
        .querySelectorAll('[lang]:not([title], [lang=""])')
        .forEach(giveTitle);
}
// @license-end
