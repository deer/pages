"""
This code is incorrect, by the way!!
But still here for... educational purposes...
"""

class MpqStg6Row:
    def __init__(self, cols=4):
        self._platforms = [set(range(1, cols + 1)) for _ in range(cols)]

    def test(self, plat, pc, result):
        """
        Have the PC ``pc`` (given as an index 1, 2, 3, or 4, where you are 1)
        test the platform ``plat`` (given as an index 1, 2, 3, or 4, from left
        to right), with ``result`` being ``True`` if the platform was correct,
        and ``False`` otherwise.

        :returns: ``None``.
        """
        if result:
            for i, plat_set in enumerate(self._platforms, start=1):
                if i == plat:
                    plat_set.clear()
                    plat_set.add(pc)
                else:
                    self._remove_pc_from_set(i, pc)
        else:
            self._remove_pc_from_set(plat, pc)

    def _remove_pc_from_set(self, plat, pc):
        plat_set = self._platforms[plat - 1]

        plat_set_len_old = len(plat_set)
        plat_set.discard(pc)
        # Did removing this PC from ``plat_set`` simultaneously determine the
        # PC for whom this platform is correct?
        if len(plat_set) == 1 and plat_set_len_old > 1:
            pc1 = tuple(plat_set)[0]
            self.test(plat, pc1, True)

    def p(self, pc=1):
        """
        :returns: A tuple of the probabilities of each platform being correct
                  for the PC ``pc``.
        """
        scores = tuple(
            (1.0 if pc in plat_set else 0.0) / len(plat_set)
            for plat_set in self._platforms
        )
        score_sum = sum(scores)
        return tuple(s / score_sum for s in scores)

    def platform(self, plat):
        """
        :returns: An immutable view into the PC set associated with platform
                  ``plat``.
        """
        return frozenset(self._platforms[plat - 1])

    def platforms(self):
        """
        :returns: An immutable view into the platforms’ PC sets.
        """
        return tuple(frozenset(s) for s in self._platforms)
