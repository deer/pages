from fractions import Fraction as Q


class MpqStg6Row:
    def __init__(self, cols=4):
        self._ps = [[Q(1, cols)] * cols for _ in range(cols)]

    def test(self, plat, pc, result):
        """
        Have the PC ``pc`` (given as an index 1, 2, 3, or 4, where you are 1)
        test the platform ``plat`` (given as an index 1, 2, 3, or 4, from left
        to right), with ``result`` being ``True`` if the platform was correct,
        and ``False`` otherwise.

        :returns: ``None``.
        """
        plat_m1 = plat - 1
        q = tuple(self._ps[pc - 1])
        for i, p in enumerate(self._ps, start=1):
            p_t = p[plat_m1]
            # pylint: disable=consider-using-enumerate
            match i == pc, result:
                # [Case 1: We test the correct platform.]
                case True, True:
                    for j in range(len(p)):
                        p[j] = Q(0)
                    p[plat_m1] = Q(1)

                # [Case 2: We test an incorrect platform.]
                case True, False:
                    for j in range(len(p)):
                        if j == plat_m1:
                            p[j] = Q(0)
                        else:
                            p[j] /= Q(1) - p_t

                # [Case 3: Someone else tests the platform that is correct for
                #          them.]
                case False, True:
                    for j in range(len(p)):
                        if j == plat_m1:
                            p[j] = Q(0)
                        else:
                            p[j] /= Q(1) - q[j]

                # [Case 4: Someone else tests a platform that is incorrect for
                #          them.]
                case False, False:
                    q_t = q[plat_m1]
                    for j in range(len(p)):
                        print(i, p, end="  $  ")
                        print(
                            f"i = {j}; t = {plat_m1}; p[i] = {p[j]}; p[t] = {p_t}; q[i] = {q[j]}; q[t] = {q_t};",
                            end="  %  ",
                        )
                        x = (
                            Q(1) if j == plat_m1 else Q(1) - q_t / (Q(1) - p_t)
                        ) / (Q(1) - q_t)
                        p[j] *= x
                        print(x)

    def p(self, pc=1):
        """
        :returns: A tuple of the probabilities of each platform being correct
                  for the PC ``pc``.
        """
        return tuple(self._ps[pc - 1])

    def foobar(self):
        col_sums = [Q(0)] * len(self._ps[0])
        for p in self._ps:
            if sum(p) != Q(1):
                return False
            for i, prob in enumerate(p):
                col_sums[i] += prob
        for s in col_sums:
            if s != Q(1):
                return False
        return True


row = MpqStg6Row()
for k in range(1, 4 + 1):
    print(row.p(k))
print(f"~~~ {row.foobar()}")

###
#
# row.test(1, 2, True)
# for k in range(1, 4 + 1):
#    print(row.p(k))
# print("~~~")
#
# row.test(3, 3, False)
# for k in range(1, 4 + 1):
#    print(row.p(k))
# print("~~~")
#
# row.test(2, 4, False)
# for k in range(1, 4 + 1):
#    print(row.p(k))
# print("~~~")
#
###

row.test(1, 2, False)
for k in range(1, 4 + 1):
    print(row.p(k))
print(f"~~~ {row.foobar()}")

row.test(1, 2, False)
for k in range(1, 4 + 1):
    print(row.p(k))
print(f"~~~ {row.foobar()}")

row.test(1, 3, False)
for k in range(1, 4 + 1):
    print(row.p(k))
print(f"~~~ {row.foobar()}")

row.test(1, 3, False)
for k in range(1, 4 + 1):
    print(row.p(k))
print(f"~~~ {row.foobar()}")

row.test(2, 1, False)
for k in range(1, 4 + 1):
    print(row.p(k))
print(f"~~~ {row.foobar()}")
