import matplotlib
import matplotlib.pyplot as plt
import numpy as np

METADATA = {"Creator": None, "Date": None, "Format": None, "Type": None}
TRANSPARENT = "#00000000"
TRANSPARENT_BLACK = "#00000080"
SADPAD = r"\ \:"

TEN_ON_NINE = 10.0 / 9.0
AVOIDS = {
    "warrior": 25,
    "wizard": 46,
    "gunslinger": 78,
    "hunter": 122,
    "brawler": 138,
    "assassin": 235,
}
ACCBUFFS = {
    "Sniper Potion": 5,
    "Bless": 20,
    "Victoria’s Amorian Basket": 40,
}
BASEACC = 86.2
MOBS = [("Jrog", 80, 30, 65), ("Grog3", 95, 35, 70), ("Ergoth", 115, 18, 80)]


def dodgeChancePhy(acc, avoid, isThief):
    d = avoid / (4.5 * acc)
    low, high = (0.05, 0.95) if isThief else (0.02, 0.8)

    return min(max(d, low), high)


def dodgeChanceMag(acc, avoid):
    return min(max(TEN_ON_NINE - acc / (0.9 * avoid), 0.0), 1.0)


def hitChancePhy(acc, avoid, d):
    hc = acc / ((1.84 + 0.07 * d) * avoid) - 1.0

    return min(max(hc, 0.0), 1.0)


matplotlib.rcParams.update(
    {
        "figure.autolayout": True,
        "mathtext.fontset": "cm",
    }
)

#### Physical dodge-chances ####

fig, ax = plt.subplots(figsize=(8.0, 6.0))

avoids = np.arange(20, 260 + 1)
for acc in [70, 100, 175, 230]:
    if acc in [70, 230]:
        hcp_v = np.vectorize(lambda avoid: dodgeChancePhy(acc, avoid, True))
        d = hcp_v(avoids)
        ax.plot(avoids, d, "--", color=TRANSPARENT_BLACK)

    hcp_v = np.vectorize(lambda avoid: dodgeChancePhy(acc, avoid, False))
    d = hcp_v(avoids)
    ax.plot(
        avoids,
        d,
        label=rf"$\mathrm{{ACC}} = {SADPAD if acc < 100 else ''}{acc}$",
    )

ax.set_xticks(np.arange(20, 260 + 1, 15))
ax.set_yticks(np.linspace(0, 1, 10 - 1))

ax.set_xlabel(r"$\mathrm{AVOID}$", size=20)
ax.set_ylabel(r"$\mathrm{physical}\ \,\mathrm{dodge{-}chance}$", size=20)

avoid_values = list(AVOIDS.values())
ax.vlines(
    avoid_values,
    [0] * len(avoid_values),
    [dodgeChancePhy(70, v, False) for v in avoid_values],
    linestyles="dotted",
    colors=TRANSPARENT_BLACK,
)

for c, avoids in AVOIDS.items():
    xy = avoids, dodgeChancePhy(70, avoids, False)
    ax.annotate(
        c,
        xy,
        xytext=(xy[0], xy[1] + 0.02),
        ha="center",
        va="center",
        rotation=30,
    )

ax.set_xmargin(0)
ax.set_ymargin(0)
ax.set_ylim(0, 1)

ax.spines["top"].set_color(TRANSPARENT)
ax.spines["right"].set_color(TRANSPARENT)

ax.legend(fontsize="xx-large", frameon=False)

fig.savefig("phy_avoid_plot.svg", format="svg", metadata=METADATA)

#### Magical dodge-chances ####

fig, ax = plt.subplots(figsize=(8.0, 6.0))

avoids = np.arange(20, 260 + 1)
for acc in [70, 100, 175, 230]:
    hcp_v = np.vectorize(lambda avoid: dodgeChanceMag(acc, avoid))
    d = hcp_v(avoids)
    ax.plot(
        avoids,
        d,
        label=rf"$\mathrm{{ACC}} = {SADPAD if acc < 100 else ''}{acc}$",
    )

ax.set_xticks(np.arange(20, 260 + 1, 15))
ax.set_yticks(np.linspace(0, 1, 10 - 1))

ax.set_xlabel(r"$\mathrm{AVOID}$", size=20)
ax.set_ylabel(r"$\mathrm{magical}\ \,\mathrm{dodge{-}chance}$", size=20)

avoid_values = list(AVOIDS.values())
ax.vlines(
    avoid_values,
    [0] * len(avoid_values),
    [dodgeChanceMag(70, v) for v in avoid_values],
    linestyles="dotted",
    colors=TRANSPARENT_BLACK,
)

for c, avoids in AVOIDS.items():
    xy = avoids, dodgeChanceMag(70, avoids)
    ax.annotate(c, xy, xytext=(xy[0], xy[1] + 0.015), ha="center", va="center")

ax.set_xmargin(0)
ax.set_ymargin(0)
ax.set_ylim(0, 1)

ax.spines["top"].set_color(TRANSPARENT)
ax.spines["right"].set_color(TRANSPARENT)

ax.legend(fontsize="xx-large", frameon=False)

fig.savefig("mag_avoid_plot.svg", format="svg", metadata=METADATA)

#### Jrog hit-chances ####

for mobName, mobLv, mobAvd,lowLv in MOBS:
    fig, ax = plt.subplots(figsize=(8.0, 6.0))

    levels = np.arange(lowLv, mobLv + 3)
    for buffName, accBuff in ACCBUFFS.items():
        hcp_v = np.vectorize(
            lambda lv: hitChancePhy(
                BASEACC + accBuff, mobAvd, max(mobLv - lv, 0)
            )
        )
        d = hcp_v(levels)
        ax.plot(levels, d, label=buffName)

    ax.set_xticks(np.arange(lowLv, mobLv + 3, 5))
    ax.set_yticks(np.linspace(0, 1, 10 - 1))

    ax.set_xlabel("level", size=20)
    ax.set_ylabel(f"{mobName} hit-chance", size=20)

    ax.set_xmargin(0)
    # ax.set_ymargin(0)
    ax.set_ylim(0, 1.05)

    ax.spines["top"].set_color(TRANSPARENT)
    ax.spines["right"].set_color(TRANSPARENT)

    ax.legend(fontsize="xx-large", frameon=False)

    fig.savefig(
        f"{mobName.lower()}_hit-chance_plot.svg",
        format="svg",
        metadata=METADATA,
    )
