from math import ceil

MODELS = {
    "fighter": ((345, 285), (139, 139), 382, "warrior", (406, 69, 6, 6), 5414),
    "page": ((345, 285), (139, 139), 382, "warrior", (406, 69, 6, 6), 5089),
    "spear(wo)man": (
        (345, 285),
        (179, 139),
        382,
        "warrior",
        (406, 69, 6, 6),
        5089,
    ),
    "F/P wizard": ((222, 182), (634, 634), 212, "mage", (6, 6, 406, 78), 1116),
    "I/L wizard": ((222, 182), (634, 634), 212, "mage", (6, 6, 406, 78), 1116),
    "cleric": ((222, 182), (654, 634), 212, "mage", (6, 6, 406, 78), 1116),
    "hunter": ((206, 206), (115, 115), 238, "archer", (75, 398, 6, 6), 2344),
    "crossbow(o)man": (
        (206, 206),
        (115, 115),
        238,
        "archer",
        (70, 403, 6, 6),
        2344,
    ),
    "assassin": ((181, 181), (115, 115), 291, "thief", (6, 113, 6, 364), 2344),
    "bandit": ((186, 186), (115, 115), 291, "thief", (55, 113, 6, 323), 2344),
    "brawler": ((210, 210), (115, 115), 239, "pirate", (385, 92, 6, 6), 3910),
    "gunslinger": (
        (200, 200),
        (115, 115),
        239,
        "pirate",
        (74, 397, 6, 6),
        2464,
    ),
}
MOBS = {
    "Jrog": (80, 450, 605),
    "Grog3": (95, 300, 430),
    "Ergoth": (115, 700, 700),
}


class Stats:
    def __init__(
        self, watk, matk, wdef, mdef, pdd, pLv, mLv, arch, stre, dex, inte, luk
    ):
        self.watk = watk
        self.matk = matk
        self.wdef = wdef
        self.mdef = mdef
        self.pdd = pdd
        self.pLv = pLv
        self.mLv = mLv
        self.arch = arch
        self.stre = stre
        self.dex = dex
        self.inte = inte
        self.luk = luk


def inPhy(stats):
    """
    Incoming (from the player’s perspective) physical damage.
    """
    watkSq = float(stats.watk * stats.watk)
    d = 13 / (13 + stats.pLv - stats.mLv) if stats.pLv >= stats.mLv else 1.3
    c = (
        (
            stats.stre / 2_800 + stats.dex / 3_200
            if stats.arch == "warrior"
            else stats.stre / 2_000 + stats.dex / 2_800
        )
        + stats.inte / 7_200
        + stats.luk / 3_200
    )
    b = (
        c * 28 / 45 + stats.pLv * 7 / 13_000 + 0.196
        if stats.wdef >= stats.pdd
        else d * (c + stats.pLv / 550 + 0.28)
    )
    a = c + 0.28
    sub = stats.wdef * a + (stats.wdef - stats.pdd) * b

    return (watkSq * 0.008 - sub, watkSq * 0.008_5 - sub)


def inMag(stats):
    """
    Incoming (from the player’s perspective) magical damage.
    """
    matkSq = float(stats.matk * stats.matk)
    k = 1.2 if stats.arch == "mage" else 1.0
    sub = k * (
        stats.mdef / 4 + stats.stre / 28 + stats.dex / 24 + stats.luk / 20
    )

    return (matkSq * 0.007_5 - sub, matkSq * 0.008 - sub)


if __name__ == "__main__":
    for (
        modelName,
        (wdef, mdef, pdd, arch, (stre, dex, inte, luk), maxhp),
    ) in MODELS.items():
        stats = Stats(
            0, 0, wdef[0], mdef[0], pdd, 80, 0, arch, stre, dex, inte, luk
        )
        print(f"| {modelName} |", end="")
        for (mobName, (mLv, watk, matk)) in MOBS.items():
            stats.mLv = mLv
            stats.watk = watk
            stats.matk = matk

            stats.wdef = wdef[0]
            afterMod = 1.0
            if modelName in ["fighter", "page"]:
                afterMod = 0.6
            elif arch == "mage":
                afterMod = 0.2 * (0.7 if modelName == "cleric" else 1.0)
            dmg = ceil(inPhy(stats)[1] * afterMod)
            if dmg >= int(maxhp * 1.6):
                ot, ct = "<b>", "</b>"
            elif dmg >= maxhp:
                ot, ct = "<u>", "</u>"
            else:
                ot, ct = "", ""
            print(f" {ot}{dmg}{ct} |", end="")

            if mobName != "Jrog":
                stats.wdef = wdef[1]
                dmg = ceil(inPhy(stats)[1])
                if dmg >= maxhp:
                    ot, ct = "<b>", "</b>"
                else:
                    ot, ct = "", ""
                print(f" {ot}{dmg}{ct} |", end="")

            stats.mdef = mdef[0]
            afterMod = 1.0
            if arch == "mage":
                afterMod = 0.2
            dmg = ceil(inMag(stats)[1] * afterMod)
            if dmg >= int(maxhp * 1.6):
                ot, ct = "<b>", "</b>"
            elif dmg >= maxhp:
                ot, ct = "<u>", "</u>"
            else:
                ot, ct = "", ""
            print(f" {ot}{dmg}{ct} |", end="")

            if mobName != "Jrog":
                stats.mdef = mdef[1]
                dmg = ceil(inMag(stats)[1])
                if dmg >= maxhp:
                    ot, ct = "<b>", "</b>"
                else:
                    ot, ct = "", ""
                print(f" {ot}{dmg}{ct} |", end="")

        print()
