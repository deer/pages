import re

SECT_RE = re.compile(
    r"^- <b>(?P<term>[^:]+):</b>\s+(?P<main_def>.+)\.(?P<stat_listing>(\n\s+- <b>[A-Za-z]+:</b>\s+[0-9]+\.)+)",
    flags=re.MULTILINE,
)
STAT_RE = re.compile(r"- <b>(?P<stat>[A-Za-z]+):</b>\s+(?P<val>[0-9]+)\.")
STAT_NAMES = {
    "WATK": ("WAtk", False),
    "MATK": ("MAtk", False),
    "STR": ("STR", True),
    "DEX": ("DEX", True),
    "INT": ("INT", True),
    "LUK": ("LUK", True),
    "MAXHP": ("maxHP", False),
    "MAXMP": ("maxMP", False),
    "WDEF": ("WDef", False),
    "MDEF": ("MDef", False),
    "WACC": ("WAcc", False),
    "SPEED": ("SPEED", True),
    "JUMP": ("JUMP", True),
}

with open("./goob.txt", "r", encoding="UTF-8") as f:
    inp = f.read()

print("<dl>", end="")

for m in SECT_RE.finditer(inp):
    term = m.group("term")
    main_def = m.group("main_def")
    stat_listing = m.group("stat_listing")

    print(
        f"\n<dt>{term}</dt>\n<dd>\n\n{main_def}.\n\n<table>\n  <tbody>", end=""
    )

    for stat_m in STAT_RE.finditer(stat_listing):
        stat = stat_m.group("stat")
        val = stat_m.group("val")

        stat_spelling, all_small = STAT_NAMES[stat]

        print(
            f'\n    <tr>\n      <th scope="row" style="font-variant-caps: {"all-" if all_small else ""}small-caps;">{stat_spelling}</th>\n      <td style="text-align: right;">{val}</td>\n    </tr>',
            end="",
        )

    print("\n  </tbody>\n</table>\n\n</dd>")

print("</dl>")
