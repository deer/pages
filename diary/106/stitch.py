#!/usr/bin/env python3

import os, os.path, random, re

SPACE_RE = re.compile(r"[ \t\r\n]+")
NONSPACE_RE = re.compile(r"\S+")
NUM_RE = re.compile(r"[0-9]+")
JUST_NUM_RE = re.compile(r"\#?-?\+?[0-9,\.]+")
FN_RE = re.compile(r"[0-9\[\]\(\)\{\}\^_]+")
CHUNK_COUNT = 128
MIN_CHUNK_SIZE = 2
MAX_CHUNK_SIZE = 12

spacesplit = []
for filename in os.listdir("."):
    filepath = os.path.join(".", filename)
    if not (filepath.endswith(".txt") and os.path.isfile(filepath)):
        continue

    with open(filepath, "r", encoding="UTF-8") as f:
        for s in SPACE_RE.split(f.read()):
            if NONSPACE_RE.search(s) is None:
                continue
            if NUM_RE.search(s) is not None:
                if JUST_NUM_RE.fullmatch(s) is None:
                    spacesplit.append(FN_RE.sub("", s))
                    continue
            spacesplit.append(s)

chunks = []
for i in range(CHUNK_COUNT):
    chunk_start = random.randint(0, len(spacesplit) - MAX_CHUNK_SIZE - 1)
    chunk_end = chunk_start + random.randint(MIN_CHUNK_SIZE, MAX_CHUNK_SIZE)
    chunk = " ".join(spacesplit[chunk_start:chunk_end])

    if random.random() < 0.25:
        chunk = chunk.upper()

    chunks.append(chunk)

print(" ".join(chunks))
