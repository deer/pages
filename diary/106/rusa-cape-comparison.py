import math
import matplotlib.pyplot as plt
import numpy as np

METADATA = {"Creator": None, "Date": None, "Format": None, "Type": None}
WATK_BUFFS = [0, 20, 25, 30, 35, 40, 60, 100, 120]
CHC = [
    21275.148,
    25320.929,
    26332.374,
    27343.819,
    28355.264,
    29366.710,
    33412.490,
    41504.052,
    45549.832,
]
PGC = [
    22304.789,
    26160.389,
    27124.289,
    28088.189,
    29052.089,
    30015.989,
    33871.589,
    41582.789,
    45438.389,
]

fig, ax = plt.subplots(figsize=(9.0, 6.0), layout="constrained")

ax.set_xlabel("WATK buff")
ax.set_ylabel("DPM (millions)")

ax.set_ylim(1.25, 2.75)
ax.yaxis.set_ticks(np.linspace(1.25, 2.75, 10 + 1))

(chc_plot,) = ax.plot(
    WATK_BUFFS, [60 * x / 1_000_000 for x in CHC], label="CHC"
)
(pgc_plot,) = ax.plot(
    WATK_BUFFS, [60 * x / 1_000_000 for x in PGC], label="PGC"
)
ax.legend(handles=[chc_plot, pgc_plot])

ax.spines[["right", "top"]].set_visible(False)

plt.savefig("rusa-cape-comparison.svg", format="svg", metadata=METADATA)
