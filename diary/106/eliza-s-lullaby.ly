\version "2.22.1"

\paper {
  #(set-paper-size "a5")
  indent = 0
  system-system-spacing =
    #'((basic-distance . 10)
       (minimum-distance . 4)
       (padding . 1)
       (stretchability . 50))
}

\header {
  title = \markup { \huge \bold \italic "Lullaby." }
  composer = \markup { \smallCaps "Eliza." }
  tagline = #f
}

\score {
  \new MensuralStaff {
    \clef "mensural-c1"
    \time 6/4
    \override NoteHead.style = #'petrucci
    \override Flag.style = #'mensural
    \override Score.BarNumber.break-visibility = ##(#f #f #f)

    \fixed c' {
      c1. g1 e2
      d1. f1 a2
      g1. f1 e2
      f1 e d
      \break

      c1. g1 e2
      d1. f1 a2
      g1. f1 e2
      f1 g b\fermata
      \break

      c1. f1 b2
      e1. a1 d2
      g1. f1 a2
      b1 a b
      \break

      c e\longa\fermata
      \undo \hide Staff.BarLine
      \bar "|."
    }
  }
}
