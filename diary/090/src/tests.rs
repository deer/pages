#![cfg(test)]

use crate::pmf;

#[test]
fn pmf_test() {
    assert_eq!(pmf(20, 3), 0.066_128_584_441_193_38);
}
