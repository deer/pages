#![forbid(unsafe_code)]
#![deny(clippy::pedantic)]

mod tests;

use rustc_hash::FxHashMap as Map;

#[must_use]
pub fn pmf(k: u32, n: u32) -> f64 {
    if k == 0 {
        if n == 0 {
            1.0
        } else {
            0.0
        }
    } else if n < 1 {
        0.0
    } else {
        pmf_inner(k, n, 0)
    }
}

#[must_use]
fn pmf_inner(k: u32, n: u32, d: u32) -> f64 {
    fn f(cache: &mut Map<(u32, u32), f64>, k: u32, n: u32, d: u32) -> f64 {
        if let Some(x) = cache.get(&(n, d)) {
            return *x;
        }

        let denom = f64::from(2 * k - d + 1);

        if n == 1 {
            2.0 / denom
        } else {
            let sum: f64 = (d + 1..k).map(|i| f(cache, k, n - 1, i)).sum();
            let r =
                (2.0 * sum + f64::from(d + 1) * f(cache, k, n - 1, d)) / denom;
            cache.insert((n, d), r);

            r
        }
    }

    let mut cache = Map::default();

    f(&mut cache, k, n, d)
}
