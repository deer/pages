import math
from pbc import pmf_float

PRECALCED_ENTROPY = [
    0.0,
    0.9547712524422186,
    1.5380028326249204,
    1.8625509411362713,
    2.0928053875323416,
    2.2730472263050148,
    2.4219462531525195,
    2.549211140534328,
    2.6605701687259486,
    2.7597026873844195,
    2.849120949124844,
    2.9306212772978335,
    3.005535224434021,
    3.0748788129963143,
    3.1394459475293344,
    3.199869416667452,
    3.256662161011963,
    3.3102460360629036,
    3.3609723780723635,
    3.4091370378045878,
    3.4549915849998705,
    3.4987518026510114,
    3.540604225064083,
    3.5807112389726883,
    3.619215112429011,
    3.656241212209541,
    3.6919005991318725,
    3.726292140865961,
    3.7595042464798376,
    3.7916163015133013,
    3.8226998638057847,
    3.852819666586721,
    3.8820344650870826,
    3.910397755189974,
    3.93795838673486,
    3.96476108954685,
    3.990846926735502,
    4.016253687048119,
    4.0410162258867945,
    4.065166762871379,
    4.0887351424496305,
    4.111749062946124,
    4.134234278542467,
    4.156214777950882,
    4.177712942945922,
    4.198749689426636,
    4.2193445932768086,
    4.239516002952762,
    4.259281140449268,
    4.27865619205724,
    4.297656390131044,
    4.316296086916333,
    4.334588821347844,
    4.352547379608117,
    4.37018385013519,
    4.387509673680017,
    4.404535688940854,
    4.42127217423594,
]


def estimate_expectation(k, epsilon):
    e = 0.0
    cumu_prob = 0.0
    n = 1
    while cumu_prob < 1.0 - epsilon:
        prob = pmf_float(k, n)
        old_cumu_prob = cumu_prob
        cumu_prob += prob
        e += prob * float(n)
        n += 1

        if old_cumu_prob == cumu_prob:
            break

    return e


def estimate_expectation_sq(k, epsilon):
    e = 0.0
    cumu_prob = 0.0
    n = 1
    while cumu_prob < 1.0 - epsilon:
        prob = pmf_float(k, n)
        old_cumu_prob = cumu_prob
        cumu_prob += prob
        e += prob * float(n * n)
        n += 1

        if old_cumu_prob == cumu_prob:
            break

    return e


def estimate_entropy(k, epsilon):
    eta = 0.0
    cumu_prob = 0.0
    n = 1
    while cumu_prob < 1.0 - epsilon:
        prob = pmf_float(k, n)
        old_cumu_prob = cumu_prob
        cumu_prob += prob
        try:
            eta += prob * math.log(prob)
        except:
            pass
        n += 1

        if old_cumu_prob == cumu_prob:
            break

    return -eta


def mode(k):
    p = 0.0
    n = 1
    while True:
        prob = pmf_float(k, n)
        if prob > p:
            p = prob
        else:
            break

        n += 1

    return n - 1
