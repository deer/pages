use criterion::{black_box, criterion_group, criterion_main, Criterion};
use rbc::pmf;

fn criterion_benchmark(c: &mut Criterion) {
    c.bench_function("pmf(20, 24)", |b| {
        b.iter(|| pmf(black_box(20), black_box(24)))
    });
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
