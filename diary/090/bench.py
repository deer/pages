import convergence
import timeit

print("Benchmarking using `timeit.repeat`...")
pmf2_time = min(
    timeit.repeat(
        "convergence.pmf2(20, 24)",
        "import convergence",
        repeat=6,
        number=100,
    )
)
print(f"pmf2: {pmf2_time}")
pmf_time = min(
    timeit.repeat(
        "convergence.pmf(20, 24)",
        "import convergence",
        repeat=6,
        number=100,
    )
)
print(f"pmf: {pmf_time}")
pmf_float_time = min(
    timeit.repeat(
        "convergence.pmf_float(20, 24)",
        "import convergence",
        repeat=6,
        number=100,
    )
)
print(f"pmf_float: {pmf_float_time}")
