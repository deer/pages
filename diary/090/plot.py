from estimation import PRECALCED_ENTROPY
import math
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from rbc import expectation, expectation_approx, pmf, pmf_float
from scipy.interpolate import make_interp_spline

METADATA = {"Creator": None, "Date": None, "Format": None, "Type": None}
TRANSPARENT = "#00000000"
TRANSPARENT_BLACK = "#00000080"

matplotlib.rcParams.update(
    {
        "figure.autolayout": True,
        "mathtext.fontset": "cm",
    }
)

#### k=20 PMF plot ####

k = 20
n_upper = 16
n = np.arange(1, n_upper)
pmf_v = np.vectorize(lambda n: pmf(k, n))
p = pmf_v(n)
spline = make_interp_spline(n, p, k=3)
n_smooth = np.linspace(1, n_upper - 1, 24**2)
p_smooth = spline(n_smooth)

fig, ax = plt.subplots(figsize=(8.0, 6.0))
ax.plot(n_smooth, p_smooth)

ax.set_xticks(np.arange(1, n_upper))
ax.set_yticks(np.linspace(0, 0.07, 15))

ax.set_xlabel(r"$n$", size=20)
ax.set_ylabel(r"$\mathrm{RBC}(20, n)$", size=20)

ax.vlines(
    n, [0] * (n_upper - 1), p, linestyles="dotted", colors=TRANSPARENT_BLACK
)

ax.set_ymargin(0)
ax.set_ylim(0, 0.07)

ax.spines["top"].set_color(TRANSPARENT)
ax.spines["right"].set_color(TRANSPARENT)

ax.fill_between(n_smooth, p_smooth, color="#1f77b440")

fig.savefig("rbc_plot.svg", format="svg", metadata=METADATA)

#### k=[10,20,40,80] PMF plot large ####

COLOUR_CYCLE = [x["color"] for x in matplotlib.rcParams["axes.prop_cycle"]]

fig, ax = plt.subplots(figsize=(18.0, 6.0))

n_upper = 101
n = np.arange(1, n_upper)
n_smooth = np.linspace(1.0, n_upper - 1.0, 1_000)
x_ticks = np.concatenate(([1], np.arange(5, n_upper, 5)))

maxes = [0.0] * len(x_ticks)
for i, k in enumerate([10, 20, 40, 80]):
    pmf_v = np.vectorize(lambda n: pmf_float(k, n))
    p = pmf_v(n)
    spline = make_interp_spline(n, p, k=3)
    p_smooth = spline(n_smooth)

    ax.plot(n_smooth, p_smooth, label=rf"$k = {k}$")

    ax.fill_between(
        n_smooth, p_smooth, color=COLOUR_CYCLE[i] + "40", antialiased=True
    )
    maxes = np.maximum(maxes, pmf_v(x_ticks))

ax.set_xticks(x_ticks)
ax.set_yticks(np.linspace(0, 0.12, 13))

ax.set_xlabel(r"$n$", size=20)
ax.set_ylabel(r"$\mathrm{RBC}(k, n)$", size=20)

ax.set_xmargin(1 / 64)
ax.set_ymargin(0)
ax.set_ylim(0, 0.12)

ax.vlines(
    x_ticks,
    [0] * len(x_ticks),
    maxes,
    linestyles="dotted",
    colors=TRANSPARENT_BLACK,
)

ax.spines["top"].set_color(TRANSPARENT)
ax.spines["right"].set_color(TRANSPARENT)

ax.legend(fontsize="xx-large", frameon=False)

fig.savefig("rbc_plot_large.svg", format="svg", metadata=METADATA)

#### Expectation approximation plot ####

k_upper = 21
linspc = np.linspace(0.0, k_upper - 1.0, 64 * (k_upper - 1))
intspc = np.arange(0, k_upper)
exp_v = np.vectorize(expectation)
exp_approx_v = np.vectorize(expectation_approx)
exp = exp_v(intspc)
exp_approx = exp_approx_v(linspc)

fig, (ax0, ax1, ax2) = plt.subplots(3, 1, figsize=(8.0, 18.0))
ax0.plot(intspc, exp, "o")
ax0.plot(linspc, exp_approx)

ax0.set_xticks(np.arange(0, k_upper))
ax0.set_yticks(np.arange(0, 14))

ax0.set_xlabel(r"$k$", size=20)
ax0.set_ylabel(r"$\mathrm{E}[N(k)]$", size=20)

ax0.vlines(
    intspc,
    [0] * len(intspc),
    exp,
    linestyles="dotted",
    colors=TRANSPARENT_BLACK,
)

ax0.set_ymargin(0)
ax0.set_ylim(0, 13)

ax0.spines["top"].set_color(TRANSPARENT)
ax0.spines["right"].set_color(TRANSPARENT)

abs_err = exp_approx_v(intspc) - exp

ax1.plot(intspc, abs_err, "o-")

ax1.set_xticks(np.arange(0, k_upper))

ax1.set_xlabel(r"$k$", size=20)
ax1.set_ylabel("absolute error", size=20)

ax1.set_ymargin(0)
ax1.set_ylim(0, 0.04)

ax1.spines["top"].set_color(TRANSPARENT)
ax1.spines["right"].set_color(TRANSPARENT)

rel_err = abs_err[1:] / exp[1:]

ax2.plot(intspc[1:], 100.0 * rel_err, "o-")

ax2.yaxis.set_major_formatter(matplotlib.ticker.PercentFormatter())

ax2.set_xticks(np.arange(1, k_upper))

ax2.set_xlabel(r"$k$", size=20)
ax2.set_ylabel("relative error", size=20)

ax2.set_ymargin(0)
ax2.set_ylim(0, 3)

ax2.spines["top"].set_color(TRANSPARENT)
ax2.spines["right"].set_color(TRANSPARENT)

fig.savefig("e_approx_plot.svg", format="svg", metadata=METADATA)

### Entropy estimation & approximation plot ###

fig, ax = plt.subplots(figsize=(9.0, 6.0))
linspc = np.linspace(0, len(PRECALCED_ENTROPY) - 1.0, 24**2)
spline = make_interp_spline(
    np.arange(0, len(PRECALCED_ENTROPY)), PRECALCED_ENTROPY, k=3
)
p_smooth = spline(linspc)
ax.plot(linspc, p_smooth)
ax.plot(
    linspc,
    list(
        map(
            lambda k: (
                9.0 * (math.log(k + 1) + math.log(math.log(k + 2))) + 4.0
            )
            / 12.0,
            linspc,
        )
    ),
    color="#ee443380",
    ls="--",
    lw=4.0,
)

ax.set_xlabel(r"$k$", size=20)
ax.set_ylabel(r"$\mathrm{H}(N(k))$ (nat)", size=20)

ax.set_xticks(np.arange(0, 59, 5))
ax.set_ymargin(0.0)
ax.set_ylim(0.0, 4.55)
ax.set_yticks(np.linspace(0.0, 4.5, 10))

sndy_yax = ax.secondary_yaxis(
    "right", functions=(lambda x: x / math.log(2), lambda x: x * math.log(2))
)
sndy_yax.set_ylabel(r"$\mathrm{H}(N(k))$ (Sh)", size=20)
sndy_yax.set_yticks(np.linspace(0.0, 6.5, 14))

ax.spines["top"].set_color(TRANSPARENT)

fig.savefig("entropy_plot.svg", format="svg", metadata=METADATA)
