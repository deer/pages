import csv
import math
import matplotlib.pyplot as plt
import numpy as np

PREAMBLE = """\
<table>
<thead>
<tr>
<th scope="col">item</th>
<th scope="col">pulls</th>
<th scope="col">quantity</th>
</tr>
</thead>
<tbody>"""
POSTAMBLE = """\
</tbody>
<tfoot>
<tr>
<th scope="row">totals</th>
<td style="text-align: right;">"""
ITEMS = {
    "rare gach": ("Rare Gachapon Ticket", "cash?id=5220010"),
    "mcp1": ("Mysterious Coin Pouch 1", "use?id=2029200"),
    "mcp2": ("Mysterious Coin Pouch 2", "use?id=2029201"),
    "mcp3": ("Mysterious Coin Pouch 3", "use?id=2029202"),
    "maple shield": ("Maple Shield", "equip?id=1092030"),
    "pac": ("Pink Adventurer Cape", "equip?id=1102041"),
    "puac": ("Purple Adventurer Cape", "equip?id=1102042"),
    "pgc": ("Pink Gaia Cape", "equip?id=1102084"),
    "pugc": ("Purple Gaia Cape", "equip?id=1102086"),
    "maple hat (30)": ("Maple Hat [level 30]", "equip?id=1002510"),
    "maple hat (70)": ("Maple Hat [level 70]", "equip?id=1002511"),
    "maple hat (90)": ("Maple Hat [level 90]", "equip?id=1002758"),
    "maple ear (40)": ("Maple Earring [level 40]", "equip?id=1032041"),
    "maple ear (70)": ("Maple Earring [level 70]", "equip?id=1032042"),
    "maple cape (40)": ("Maple Cape [level 40]", "equip?id=1102167"),
    "maple cape (70)": ("Maple Cape [level 70]", "equip?id=1102168"),
    "mushroom box": ("Retro Orange Mushroom Box", None),
    "slime box": ("Retro Slime Box", None),
    "yeti box": ("Retro Yeti Box", None),
    "pink bean box": ("Retro Pink Bean Box", None),
    "candy": ("Candy", "use?id=2022033"),
    "mysterious candy": ("Mysterious Candy", "use?id=2022229"),
    "chocolate cake": ("Chocolate Cake", "use?id=2022565"),
    "strawberry cake": ("Strawberry Shortcake", "use?id=2022566"),
    "cream cake": ("Cream Shortcake", "use?id=2022567"),
    "cookie": ("Cookie", "use?id=2022031"),
    "mapleade": ("Mapleade", "use?id=2022195"),
    "cheesecake": ("Cheesecake", "use?id=2020016"),
    "birthday cake": ("Birthday Cake", "use?id=2020032"),
    "css3": ("Clean Slate Scroll 3%", "use?id=2049001"),
    "tp coke": ("Teleport Coke", "cash?id=5040001"),
    "pixel bloom of lucid": ("Pixel Bloom of Lucid", None),
    "pixel first maple tree": ("Pixel First Maple Tree", None),
}
METADATA = {"Creator": None, "Date": None, "Format": None, "Type": None}


def thousands(n):
    return f"{n:,}".replace(",", "&#x202f;")


# n_s is the number of successes.
#
# The magic constant 1.96 is the z-score corresponding to an area (to the left)
# of 0.975 = 0.95 + (1 - 0.95)/2. Thus, it corresponds to a confidence level of
# 95%.
def wilson_interval(n_s, n, z=1.96):
    zz = z * z
    n_f = n - n_s

    centre = (n_s + 0.5 * zz) / (n + zz)
    err = z * math.sqrt(n_s * n_f / n + zz / 4) / (n + zz)

    return centre - err, centre + err


stats = []
with open("raffle.csv", "r", encoding="UTF-8") as f:
    rdr = csv.reader(f)
    for i, row in enumerate(rdr):
        if i == 0:
            continue
        stats.append((row[0], int(row[1]), int(row[2])))

print(PREAMBLE)
tpulls, tqty = 0, 0
stats_sorted = sorted(stats, key=lambda r: (r[1], r[2], r[0]), reverse=True)
for item, pulls, per in stats_sorted:
    name, frag = ITEMS[item]
    aopen, aclose = "", ""
    if frag is not None:
        aopen, aclose = (
            f"""<a href="https://maplelegends.com/lib/{frag}">""",
            "</a>",
        )
    qty = pulls * per
    print(
        f"""<tr>\n<td>{aopen}{name}{aclose}</td>\n<td style="text-align: right;">{thousands(pulls)}</td>\n<td style="text-align: right;">{thousands(qty)}</td>\n</tr>"""
    )
    tpulls += pulls
    tqty += qty
print(POSTAMBLE, end="")
print(
    f"""{thousands(tpulls)}</td>\n<td style="text-align: right;">{thousands(tqty)}</td>\n</tr>\n</tfoot>\n</table>"""
)

########################################

fig, ax = plt.subplots(figsize=(16.0, 8.0), layout="constrained")
ax.set_ylim(0, 140)
ax.set_ylabel("pulls")

names = [ITEMS[s[0]][0] for s in stats_sorted]
yerr = [
    (tpulls * ((n_s / tpulls) - p_l), tpulls * (p_u - (n_s / tpulls)))
    for p_l, p_u, n_s in (
        wilson_interval(n_s, tpulls) + (n_s,) for _, n_s, _ in stats_sorted
    )
]
yerr = list(zip(*yerr))
ax.bar(
    names,
    [s[1] for s in stats_sorted],
    0.75,
    label=names,
    yerr=yerr,
    ecolor="black",
    capsize=5,
)

ax.yaxis.set_ticks(range(0, 140 + 1, 10))
plt.xticks(rotation=90)

ax.spines[["right", "top"]].set_visible(False)

plt.savefig("raffle.svg", format="svg", metadata=METADATA)

########################################

fig, ax = plt.subplots(figsize=(11.0, 8.0), layout="constrained")

labels = [
    "gacha tickets",
    "MCPs",
    "Maple Shields & PACs/PGCs",
    "untradeable Maple equips",
    "Maple equipment boxes",
    "buff potions",
    "HP/MP potions",
    "CSS3s",
    "TP Cokes",
    "chairs",
]
sizes = [0] * len(labels)
for item, pulls, _ in stats_sorted:
    if item == "rare gach":
        sizes[0] += pulls
    elif item in ["mcp1", "mcp2", "mcp3"]:
        sizes[1] += pulls
    elif item in ["maple shield", "pac", "puac", "pgc", "pugc"]:
        sizes[2] += pulls
    elif item in [
        "maple hat (30)",
        "maple hat (70)",
        "maple hat (90)",
        "maple ear (40)",
        "maple ear (70)",
        "maple cape (40)",
        "maple cape (70)",
    ]:
        sizes[3] += pulls
    elif item in ["mushroom box", "slime box", "yeti box", "pink bean box"]:
        sizes[4] += pulls
    elif item in [
        "candy",
        "mysterious candy",
        "chocolate cake",
        "strawberry cake",
        "cream cake",
    ]:
        sizes[5] += pulls
    elif item in ["cookie", "mapleade", "cheesecake", "birthday cake"]:
        sizes[6] += pulls
    elif item == "css3":
        sizes[7] += pulls
    elif item == "tp coke":
        sizes[8] += pulls
    elif item in ["pixel bloom of lucid", "pixel first maple tree"]:
        sizes[9] += pulls
    else:
        assert False

zipped = list(zip(sizes, labels))
zipped.sort()
sizes, labels = zip(*zipped)

ax.pie(sizes, labels=labels, autopct="%1.1f%%", shadow=True)

plt.savefig("raffle_pie.svg", format="svg", metadata=METADATA)
