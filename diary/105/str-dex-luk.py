import math
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

psm = 4.0
lvl = 120
ap = lvl * 5 + 20
allstat = 40
raw_wacc = 20
raw_avoid = 30

allocs = [
    {
        "name": "pure-STR",
        "STR": ap - 8 + allstat,
        "DEX": 4 + allstat,
        "LUK": 4 + allstat,
    },
    {
        "name": "pure-DEX",
        "STR": 4 + allstat,
        "DEX": ap - 8 + allstat,
        "LUK": 4 + allstat,
    },
    {
        "name": "pure-LUK",
        "STR": 4 + allstat,
        "DEX": 4 + allstat,
        "LUK": ap - 8 + allstat,
    },
    {
        "name": "balanced-stats",
        "STR": ap / 3 + allstat,
        "DEX": ap / 3 + allstat,
        "LUK": ap / 3 + allstat,
    },
]

for alloc in allocs:
    dmg_min = psm * alloc["STR"] * 0.9 * 0.1 + alloc["DEX"]
    dmg_max = psm * alloc["STR"] + alloc["DEX"]
    alloc["dmg"] = (dmg_min + dmg_max) / 2
    alloc["WACC"] = alloc["DEX"] * 0.8 + alloc["LUK"] * 0.5 + raw_wacc
    alloc["AVOID"] = alloc["DEX"] * 0.25 + alloc["LUK"] * 0.5 + raw_avoid

################################################################

METADATA = {"Creator": None, "Date": None, "Format": None, "Type": None}
DMG_SCALE_FACTOR = 1.0 / 2.5
AVOID_SCALE_FACTOR = 1.6

matplotlib.style.use("dark_background")

for alloc in allocs:
    print(alloc["name"])
    print(
        f"- &#x1d5a4;&af;\\[damage\\]: {round(alloc['dmg'], 1)}.<sup>\\[12\\]</sup>"
    )
    print(f"- WACC: {round(alloc['WACC'], 1)}.")
    print(f"- AVOID: {round(alloc['AVOID'], 1)}.")
    print()

    ################################################################

    fig = plt.figure()
    ax = fig.add_subplot(projection="polar")

    poly = matplotlib.patches.Polygon(
        [
            (0.0, alloc["dmg"] * DMG_SCALE_FACTOR),
            (2.0 * np.pi / 3.0, alloc["WACC"]),
            (4.0 * np.pi / 3.0, alloc["AVOID"] * AVOID_SCALE_FACTOR),
        ],
        alpha=1.0,
        antialiased=True,
        capstyle="round",
        edgecolor="#9370DB",
        linewidth=2.0,
    )
    ax.add_patch(poly)

    ax.set_theta_offset((5 / 6) * 2.0 * np.pi)
    ax.set_rmax(600)
    ax.set_rticks(np.arange(100, 600 + 1, 100), labels=[], minor=False)
    thetatick_locs = 0, 120, 240
    ax.set_thetagrids(thetatick_locs, ("damage", "WACC", "AVOID"))
    ax.grid(True)

    # https://stackoverflow.com/questions/62180415/how-to-rotate-theta-ticklabels-in-a-matplotlib-polar-plot
    labels = []
    for label, angle in zip(ax.get_xticklabels(), thetatick_locs):
        x, y = label.get_position()
        lab = ax.text(
            x,
            y - 0.1,
            label.get_text(),
            transform=label.get_transform(),
            ha=label.get_ha(),
            va=label.get_va(),
        )
        theta = angle + (5 / 6) * 360
        theta %= 360
        if theta > 90 and theta < 270:
            theta += 180
        lab.set_rotation(theta)
        labels.append(lab)
    ax.set_xticklabels([])

    plt.savefig(f"{alloc['name']}.svg", format="svg", metadata=METADATA)

################################################################

matplotlib.style.use("default")

fig = plt.figure(figsize=(8.0, 8.0))
ax = fig.add_subplot(projection="3d")

for alloc, c in zip(allocs, ["r", "b", "k", "g"]):
    ax.plot(
        [0] + [alloc["dmg"] * DMG_SCALE_FACTOR] * 5,
        [alloc["WACC"]] * 2 + [0] + [alloc["WACC"]] * 3,
        [alloc["AVOID"] * AVOID_SCALE_FACTOR] * 4
        + [0, alloc["AVOID"] * AVOID_SCALE_FACTOR],
        f"{c}-",
        antialiased=True,
        alpha=0.75,
        linewidth=1.5,
        label=alloc["name"].replace("-", " "),
        solid_capstyle="round",
        solid_joinstyle="round",
    )

ax.set_xlim(0, 600)
ax.set_ylim(0, 600)
ax.set_zlim(0, 600)
ax.set_xticks(np.arange(0, 600 + 1, 100), labels=[], minor=False)
ax.set_yticks(np.arange(0, 600 + 1, 100), labels=[], minor=False)
ax.set_zticks(np.arange(0, 600 + 1, 100), labels=[], minor=False)
ax.set_xlabel("damage", labelpad=-7.0)
ax.set_ylabel("WACC", labelpad=-2.0)
ax.set_zlabel("AVOID", labelpad=-6.0)
ax.set_xticklabels([])
ax.set_yticklabels([])
ax.set_zticklabels([])

ax.legend()

plt.savefig("3d.svg", format="svg", metadata=METADATA)
