from datetime import datetime
from pathlib import Path
import subprocess

md_sizes = []
dir_sizes = []
creation_dates = []
for i in range(100):
    i_padded = f"{i:03}"

    md_size = Path(f"../{i_padded}/README.md").stat().st_size

    ls_files = subprocess.run(
        ["git", "ls-files", f"../{i_padded}"], capture_output=True
    ).stdout.decode("UTF-8")
    dir_size = 0
    for fp in ls_files.splitlines():
        dir_size += Path(fp).stat().st_size

    creation_date = datetime.strptime(
        subprocess.run(
            [
                "git",
                "log",
                "--follow",
                "--format=%aI",
                "--reverse",
                "--",
                f"../{i_padded}/README.md",
            ],
            capture_output=True,
        )
        .stdout.decode("UTF-8")
        .splitlines()[0]
        .replace("+00:00", "+0000"),  # shitty hack [:
        "%Y-%m-%dT%H:%M:%S%z",
    )

    md_sizes.append(md_size)
    dir_sizes.append(dir_size)
    creation_dates.append(creation_date)

md_sizes = [x / 1024 for x in md_sizes]
dir_sizes = [x / 1024 for x in dir_sizes]
creation_dates = [0.0] + [
    (creation_dates[i + 1] - creation_dates[i]).total_seconds()
    / (60 * 60 * 24)
    for i in range(len(creation_dates) - 1)
]

print(f"md_sizes = {md_sizes}")
print(f"dir_sizes = {dir_sizes}")
print(f"creation_dates = {creation_dates}")
