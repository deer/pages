import random

STARTING_WATK = 8
STARTING_SLOTS = 4
SUCCESS_THRESHOLD = 10
BAIL_THRESHOLD = 7
CS_SVC_COST = 93
ATTEMPTS = 1_000_000

successes, cs_used = 0, 0
watk_results = [0] * (SUCCESS_THRESHOLD + 5)
slots_results = [0] * STARTING_SLOTS
for _ in range(ATTEMPTS):
    watk, slots = STARTING_WATK, STARTING_SLOTS
    while slots > 0 and watk < SUCCESS_THRESHOLD and watk > BAIL_THRESHOLD:
        slots -= 1
        cs_used += 1
        if random.random() < 0.6:
            watk += random.randint(-5, 5)

    watk = max(watk, 0)
    if watk >= SUCCESS_THRESHOLD:
        successes += 1

    watk_results[watk] += 1
    slots_results[slots] += 1

avg_watk = sum(watk * n for watk, n in enumerate(watk_results)) / ATTEMPTS
avg_slots = sum(slots * n for slots, n in enumerate(slots_results)) / ATTEMPTS

print(f"Successes: {successes}")
print(f"CSes used: {cs_used}")
print(f"Avg resulting WATK: {avg_watk}")
print(f"Avg resulting slots: {avg_slots}")

success_rate = successes / ATTEMPTS
cs_per_attempt = cs_used / ATTEMPTS
cs_per_success = cs_per_attempt / success_rate
pc_per_success = cs_per_success * CS_SVC_COST

print(f"Expected cost per success: {pc_per_success} pCoins")

#### Plotting WATK results ####

import matplotlib
import matplotlib.pyplot as plt

METADATA = {"Creator": None, "Date": None, "Format": None, "Type": None}
TRANSPARENT = "#00000000"

matplotlib.rcParams.update(
    {
        "figure.autolayout": True,
        "mathtext.fontset": "cm",
    }
)

probs = [res / ATTEMPTS for res in watk_results]

fig, ax = plt.subplots(figsize=(8.0, 6.0))
ax.stem(
    range(SUCCESS_THRESHOLD + 5),
    probs,
    markerfmt="None",
    basefmt="-",
)

ax.set_xticks(range(SUCCESS_THRESHOLD + 5))
ax.set_yticks([n / 100.0 for n in range(round(max(probs) * 100.0) + 1)])

ax.set_xlabel(r"$w$", size=20)
ax.set_ylabel(r"$\mathrm{P}(WATK = w)$", size=20)

ax.set_xmargin(0.02)
ax.set_ymargin(0.0)

ax.spines["top"].set_color(TRANSPARENT)
ax.spines["right"].set_color(TRANSPARENT)

fig.savefig("watk_results_plot.svg", format="svg", metadata=METADATA)
