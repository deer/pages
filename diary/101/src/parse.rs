use winnow::{
    combinator::{alt, repeat, repeat_till0},
    token::{take_till1, take_while},
    IResult, Parser,
};

static SENTENCE_TERMINAL_TOKENS: &str = ".!?\u{2026}\u{203d}";
static INTERWORD_TOKENS: &str =
    " \n,;:/()[]{}'\"\u{201c}\u{201d}\u{2018}\u{2014}\u{2015}*";

#[derive(Debug)]
pub enum SentenceFrag<'t> {
    Terminal(&'t str),
    Interword(&'t str),
    Word(&'t str),
}

impl<'t> SentenceFrag<'t> {
    pub fn is_word(&self) -> bool {
        match self {
            Self::Word(_) => true,
            _ => false,
        }
    }
}

fn sentence_terminal(s: &str) -> IResult<&str, SentenceFrag> {
    take_while(1.., SENTENCE_TERMINAL_TOKENS)
        .map(|o| SentenceFrag::Terminal(o))
        .parse_next(s)
}

fn interword(s: &str) -> IResult<&str, SentenceFrag> {
    take_while(1.., INTERWORD_TOKENS)
        .map(|o| SentenceFrag::Interword(o))
        .parse_next(s)
}

fn word(s: &str) -> IResult<&str, SentenceFrag> {
    take_till1((SENTENCE_TERMINAL_TOKENS, INTERWORD_TOKENS))
        .map(|o| SentenceFrag::Word(o))
        .parse_next(s)
}

fn mid_sentence_frag(s: &str) -> IResult<&str, SentenceFrag> {
    alt((interword, word)).parse_next(s)
}

fn sentence(s: &str) -> IResult<&str, Vec<SentenceFrag>> {
    let (s, (mut sen, term)): (_, (Vec<_>, _)) =
        repeat_till0(mid_sentence_frag, sentence_terminal).parse_next(s)?;
    sen.push(term);
    let (s, _) = take_while(0.., " \n").parse_next(s)?;

    Ok((s, sen))
}

pub fn sentences(s: &str) -> IResult<&str, Vec<Vec<SentenceFrag>>> {
    repeat(1.., sentence).parse_next(s)
}
