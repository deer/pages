mod parse;

use parse::SentenceFrag;
use std::io::{Read, Write};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let decode = std::env::args()
        .nth(1)
        .map(|s| "--decode" == s)
        .unwrap_or_default();

    let mut inp = String::new();
    std::io::stdin().read_to_string(&mut inp)?;
    let (rest, mut sentences) = match parse::sentences(&inp) {
        Ok(s) => s,
        Err(e) => {
            eprintln!("{e}");

            std::process::exit(1)
        }
    };
    if !rest.is_empty() {
        eprintln!("[WARN] Incomplete trailing sentence: {rest:?}");
    }

    // English is a fairly analytic language with brittle word order.
    // So, let’s shatter it.
    let mut length_trace = Vec::with_capacity(sentences.len());
    for sentence in sentences.iter_mut() {
        let word_initial = sentence.first().unwrap().is_word();
        let l = sentence.len();
        let word_count = (l - if word_initial { 0 } else { 1 }) / 2;
        if word_count < 1 {
            continue;
        }
        length_trace.push(word_count - 1);

        let slot_to_ix = move |ws| {
            if word_initial {
                // w i w i … w t
                //
                // w i w i … w i t
                ws * 2 % if l % 2 == 0 { l } else { l - 1 }
            } else {
                // i w i w … i w i t
                //
                // i w i w … i w t
                1 + ws * 2 % if l % 2 == 0 { l - 2 } else { l - 1 }
            }
        };
        let swap = |(m, n)| sentence.swap(slot_to_ix(m), slot_to_ix(n));
        let chunks = length_trace.chunks_exact(2).map(|mn| {
            let &[m, n] = mn else { unreachable!() };

            (m, n)
        });
        if decode {
            chunks.rev().for_each(swap);
        } else {
            chunks.for_each(swap);
        }
    }

    let mut stdout = std::io::stdout().lock();
    for sentence in sentences {
        for sf in sentence {
            match sf {
                SentenceFrag::Word(s) => stdout.write_all(s.as_bytes())?,
                SentenceFrag::Interword(s) => {
                    stdout.write_all(s.as_bytes())?;
                }
                SentenceFrag::Terminal(s) => write!(stdout, "{s} ")?,
            }
        }
    }
    stdout.write_all(b"\n")?;
    stdout.flush()?;

    Ok(())
}
