from util import digit_sep

PREAMBLE = """<table>
  <caption><b>Table 4:</b> Theoretical diffuse DPH (in terms of <a href="https://en.wikipedia.org/wiki/Kilo-">k</a>&it;%&#x29f8;<a href="https://en.wikipedia.org/wiki/Hour">h</a>) of pure Shockwave vs. pure EB (plus SSK to fill in the gaps)</caption>
  <thead>
    <tr>
      <th scope="col">targets</th>
      <th scope="col">Shockwave</th>
      <th scope="col">EB</th>
      <th scope="col">relative</th>
    </tr>
  </thead>
  <tbody>
"""
POSTAMBLE = """  </tbody>
</table>
"""
ATTACKS = [
    ("Shockwave", 700, 1_620, 6),
    ("EB", 420, 630, 4),
]
EB_UPTIMES = [0.0, 0.611, 0.755, 0.822, 0.856, 0.883, 0.903]
HOUR_MS = 1_000 * 60 * 60

out = PREAMBLE

for n in range(1, 6 + 1):
    out += f"""    <tr>\n      <th scope="row">{n}</th>\n"""
    dphs = []
    for name, dmg_multi, period, tgt_cap in ATTACKS:
        uptime = 2 / 3 if name == "Shockwave" else EB_UPTIMES[n]
        targets = min(n, tgt_cap)
        dph = uptime * (targets * dmg_multi * HOUR_MS / period) + (
            1 - uptime
        ) * (n * 190 * HOUR_MS / 720)
        dphs.append(dph)
        out += f"""      <td style="text-align: right;">{digit_sep(str(round(dph / 1_000)))}</td>\n"""
    dphs.sort()
    rel = 100 * (dphs[1] / dphs[0] - 1)
    out += f"""      <td style="text-align: right;">+{rel:.1f}%</td>\n"""
    out += "    </tr>\n"

out += POSTAMBLE

print(out, end="")
