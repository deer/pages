def digit_sep(s, sep="&#x202f;", n=3):
    lead = len(s) % n
    ret = s[0:lead]
    for i in range(lead, len(s), n):
        if len(ret) != 0:
            ret += sep
        ret += s[i : i + n]

    return ret
