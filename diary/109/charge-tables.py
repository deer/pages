import math

PREAMBLE = """<table>
  <caption><b>Table 3:</b> Average times to charge max-level EC by purely spamming SSK (charge times are in seconds)</caption>
  <thead>
    <tr>
      <th scope="row">speed</th>
      <th scope="col" colspan="2">2</th>
      <th scope="col" colspan="2">3</th>
      <th scope="col" colspan="2">4</th>
    </tr>
    <tr>
      <th scope="col">targets</th>
      <th scope="col">charge</th>
      <th scope="col"><abbr title="uptime of Energy Charge">up</abbr></th>
      <th scope="col">charge</th>
      <th scope="col"><abbr title="uptime of Energy Charge">up</abbr></th>
      <th scope="col">charge</th>
      <th scope="col"><abbr title="uptime of Energy Charge">up</abbr></th>
    </tr>
  </thead>
  <tbody>
"""
POSTAMBLE = """  </tbody>
</table>
"""
PERIODS = [660, 720, 780]
HITS = 53
DUR = 60 * 1_000

out = PREAMBLE

for n in range(1, 6 + 1):
    out += f"""    <tr>\n      <th scope="row">{n}</th>\n"""
    for period in PERIODS:
        t_charge = math.ceil(period * math.ceil(HITS / n))
        up = 100.0 * DUR / (t_charge + DUR)
        charge = t_charge / 1_000.0
        out += f"""      <td style="text-align: right;">{charge:.1f}</td>\n"""
        out += f"""      <td style="text-align: right;">{up:.1f}%</td>\n"""
    out += "    </tr>\n"

out += POSTAMBLE

print(out, end="")
