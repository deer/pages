import matplotlib.pyplot as plt
import numpy as np
from util import digit_sep

METADATA = {"Creator": None, "Date": None, "Format": None, "Type": None}
ATTACKS = [
    ("EB", 420, [570, 630, 660], 4),
    ("Shockwave", 700, [1_500, 1_620, 1_740], 6),
    ("SSK", 190, [660, 720, 780], 6),
]

fig, ax = plt.subplots(figsize=(10.0, 6.0), layout="constrained")

ax.set_xlabel("targets")
ax.set_ylabel("DPS (%/s)")

targets = np.arange(1, 6 + 1)

for name, dmg_multi, periods, target_cap in ATTACKS:
    ax.plot(
        targets,
        [min(n, target_cap) * dmg_multi * 1_000 / periods[0] for n in targets],
        label=name,
    )

ax.spines[["right", "top"]].set_visible(False)
ax.legend()

plt.savefig("marauder-mobbing-dps.svg", format="svg", metadata=METADATA)

########

PREAMBLE = """<table>
  <caption>Summary of <abbr title="damage per second">DPS</abbr> output (in terms of %&#x29f8;s) of Shockwave vs. EB</caption>
  <thead>
    <tr>
      <th scope="row">speed category</th>
      <th scope="col" colspan="6">2</th>
      <th scope="col" colspan="6">3</th>
      <th scope="col" colspan="6">4</th>
    </tr>
    <tr>
      <th scope="row">targets</th>
      <th scope="col">1</th>
      <th scope="col">2</th>
      <th scope="col">3</th>
      <th scope="col">4</th>
      <th scope="col">5</th>
      <th scope="col">6</th>
      <th scope="col">1</th>
      <th scope="col">2</th>
      <th scope="col">3</th>
      <th scope="col">4</th>
      <th scope="col">5</th>
      <th scope="col">6</th>
      <th scope="col">1</th>
      <th scope="col">2</th>
      <th scope="col">3</th>
      <th scope="col">4</th>
      <th scope="col">5</th>
      <th scope="col">6</th>
    </tr>
  </thead>
  <tbody>
"""
POSTAMBLE = """  </tbody>
</table>
"""

out = PREAMBLE

for name, dmg_multi, periods, target_cap in ATTACKS:
    out += f"""    <tr>\n      <th scope="row">{name}</th>\n"""
    for period in periods:
        for n in range(1, 6 + 1):
            targets = min(n, target_cap)
            p = round(targets * dmg_multi * 1_000 / period)
            out += f"""      <td style="text-align: right;">{digit_sep(str(p))}</td>\n"""
    out += "    </tr>\n"

out += POSTAMBLE

print(out, end="")
