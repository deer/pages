import eta
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

METADATA = {"Creator": None, "Date": None, "Format": None, "Type": None}
TRANSPARENT = "#00000000"
TRANSPARENT_BLACK = "#00000080"

matplotlib.rcParams.update(
    {
        "figure.autolayout": True,
        "mathtext.fontset": "cm",
    }
)

for k in [8, 10]:
    vals = eta.apply_k(k, eta.eta, [1])

    fig, ax = plt.subplots(figsize=(8.0, 6.0))

    ax.stem(range(len(vals)), vals, markerfmt="None", basefmt="-")

    ax.set_xticks(np.arange(0, len(vals) + 1, len(vals) // 16))
    ax.set_yticks(np.arange(0, max(vals) + 1, 1))

    ax.set_xlabel(r"$i$", size=20)
    ax.set_ylabel(r"$\left(\eta^\infty\langle{1}\rangle\right)_i$", size=20)

    ax.set_xmargin(0.02)
    ax.set_ymargin(0)

    ax.spines["top"].set_color(TRANSPARENT)
    ax.spines["right"].set_color(TRANSPARENT)

    fig.savefig(f"pin_plot_{len(vals)}.svg", format="svg", metadata=METADATA)
