from functools import reduce


def apply_k(k, f, a):
    """Applies the function ``f`` to ``a``, ``k`` times.

    In other words, computes ``(f**k)(a)``.
    """
    return reduce(lambda a, _: f(a), range(k), a)


def p(a):
    return [e + 1 for e in a]


def eta(a):
    return a + p(a)


if __name__ == "__main__":
    print(apply_k(5, eta, [1]))
