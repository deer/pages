import matplotlib
import matplotlib.pyplot as plt
import numpy as np

TRANSPARENT = "#00000000"
TRANSPARENT_BLACK = "#00000080"

matplotlib.rcParams.update(
    {
        "figure.autolayout": True,
        "mathtext.fontset": "cm",
    }
)

x = np.linspace(-0.1, 1.1, 12**3)
h = np.exp(100 / 7 * (7 / 10 - x))
y = -100 / 7 * h / (1 + h) ** 2 - 0.1

fig, ax = plt.subplots(figsize=(8.0, 6.0))
ax.plot(x, y)
ax.set_xticks(np.arange(-0.1, 1.1, 0.1), minor=False)
ax.axvline(
    0.5, linestyle="--", linewidth=1, color=TRANSPARENT_BLACK, antialiased=True
)
ax.axvline(
    0.7, linestyle="--", linewidth=1, color=TRANSPARENT_BLACK, antialiased=True
)
ax.axvline(1, linestyle="-", linewidth=1, color="silver", antialiased=True)
ax.axhline(0, linestyle="-", linewidth=1, color="silver", antialiased=True)
ax.set_xlabel(r"$\frac{\mathrm{HP}}{\mathrm{MAXHP}} = x$", size=20)
ax.set_ylabel(r"$\frac{\mathrm{d}}{\mathrm{d}x}f_{\mathrm{new}}(x)$", size=20)

ax.spines["top"].set_color(TRANSPARENT)
ax.spines["right"].set_color(TRANSPARENT)

fig.savefig(
    "derivative.svg",
    format="svg",
    metadata={"Creator": None, "Date": None, "Format": None, "Type": None},
)
