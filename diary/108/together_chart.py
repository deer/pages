import matplotlib.pyplot as plt
import numpy as np

METADATA = {"Creator": None, "Date": None, "Format": None, "Type": None}

methods = ("GMS", "JMS", "dMS")
stats = {
    "Hamming distance": (190, 156, 125),
    "Slotwise distance": (230, 193, 159),
    "Pixel distance": (
        15_614.897_670_108_745,
        11_068.623_425_496_484,
        8_133.396_638_009_583_5,
    ),
}

fig, ax = plt.subplots(figsize=(8.0, 6.0), layout="constrained")
ax1 = ax.twinx()

x = np.arange(len(methods))
width = 1 / 4
multi = 0
for measure, vals in stats.items():
    offset = width * multi
    axis, color = (ax1, "C2") if measure == "Pixel distance" else (ax, None)
    rects = axis.bar(x + offset, vals, width, label=measure, color=color)
    axis.bar_label(rects, padding=3)
    multi += 1

color = "C0"
ax.set_ylabel("combinatorial distance (lower is better)", color=color)
ax.set_xticks(x + width, methods)
ax.set_yticks([i * 30 for i in range(8 + 1)])
ax.tick_params(axis="y", labelcolor=color)

color = "C2"
ax1.set_ylabel("pixel distance (lower is better)", color=color)
ax1.set_yticks([i * 2_000 for i in range(8 + 1)])
ax1.tick_params(axis="y", labelcolor=color)

lines, labels = ax.get_legend_handles_labels()
lines1, labels1 = ax1.get_legend_handles_labels()
ax1.legend(lines + lines1, labels + labels1, loc="best")

ax.spines[["top"]].set_visible(False)
ax1.spines[["top"]].set_visible(False)

plt.savefig("together_chart.svg", format="svg", metadata=METADATA)
