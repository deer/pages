from util import CHASE

n, k = 9, 5
sidelen, margin = 12, 4
w = n * sidelen + (n + 1) * margin
h = len(CHASE) * sidelen + (len(CHASE) + 1) * margin
r = sidelen // 4
prelude = f"""<svg version="1.1" width="{w}" height="{h}" xmlns="http://www.w3.org/2000/svg">
  <rect width="100%" height="100%" fill="#111315" />
"""
postlude = """</svg>
"""

out = prelude

for i, bs in enumerate(CHASE):
    moved_j, moving_j = -1, -1
    if i > 0:
        moved_j_bs = bs & (CHASE[i - 1] ^ bs)
        moved_j = 0
        while moved_j_bs != 1:
            moved_j_bs >>= 1
            moved_j += 1
    if i < len(CHASE) - 1:
        moving_j_bs = bs & (CHASE[i + 1] ^ bs)
        moving_j = 0
        while moving_j_bs != 1:
            moving_j_bs >>= 1
            moving_j += 1

    y = margin + i * (sidelen + margin)
    for j in range(n):
        fill = "#eaecee"
        if j == moving_j:
            fill = "#fa7070"
        if j == moved_j:
            fill = "#557c55"

        if bs & (1 << j):
            x = margin + (n - j - 1) * (sidelen + margin)
            out += f"""\n  <rect width="{sidelen}" height="{sidelen}" x="{x}" y="{y}" rx="{r}" fill="{fill}" />"""

    out += "\n"

out += postlude

with open("chase_9_5.svg", "w", encoding="UTF-8") as f:
    f.write(out)
