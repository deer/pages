from random import random

REPS = 100_000_000

costs = {}
for _ in range(REPS):
    spent, passed = 310, 0
    while passed < 3:
        spent += 130
        if random() < 0.75:
            passed += 1
        else:
            spent, passed = spent + 310, 0

    if spent in costs:
        costs[spent] += 1
    else:
        costs[spent] = 1

avg = sum((n / REPS) * c for c, n in costs.items())
print(f"average: {avg}")

#### ...plotting!!! ####

import matplotlib.pyplot as plt
import matplotlib.ticker
import numpy as np

METADATA = {"Creator": None, "Date": None, "Format": None, "Type": None}

fig, ax = plt.subplots(figsize=(12.0, 8.0), layout="constrained")

ax.set_xlabel("Pumpkin Coins spent on perfection")
ax.set_ylabel("probability")

ax.set_xlim(0, 6_000)
ax.yaxis.set_major_formatter(
    matplotlib.ticker.PercentFormatter(xmax=1.0, decimals=0)
)

xs, ys = zip(*costs.items())
xs, ys = np.array(xs), np.array(ys)
ax.bar(xs, ys / REPS, width=130 / 2)
ax.vlines([39_080 / 27], 0, max(ys) / REPS, colors="C1", linestyles="dashed")

ax.spines[["right", "top"]].set_visible(False)

plt.savefig("perf-zring-plot.svg", format="svg", metadata=METADATA)
