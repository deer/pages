import itertools
import numpy as np

#### Coördinates. ####
spawnpoint = (1629, 0)
hearth = (1818, 0)
upward_teles = 2596
bookshelves = [
    ("Jonas", 134, 0),
    ("globe", 664, 0),
    ("webs", 851, 0),
    ("snowglobe", 1186, 0),
    ("newspaper", 2419, 0),
    ("toys", 2742, 0),
    ("guard", 3271, 0),
    ("beachball", 140, 1),
    ("bird", 857, 1),
    ("empty", 1710, 1),
    ("clock", 1898, 1),
    ("vase", 2748, 1),
    ("piano", 2935, 1),
    ("Ludmilla", 3464, 1),
    ("Sophilia", 432, 2),
    ("candle", 618, 2),
    ("firewood", 1660, 2),
    ("ladder", 1999, 2),
    ("upside down", 2520, 2),
    ("gift", 2709, 2),
    ("pipe", 3220, 2),
    ("destruction", 318, 3),
    ("coat rack", 662, 3),
    ("lace", 849, 3),
    ("love letter", 1888, 3),
    ("friend", 2740, 3),
    ("tombstones", 2927, 3),
    ("mirror", 3456, 3),
]

#### Times (in ms). ####
downjump = 800
tele = 600
pixel = 8


def d(p0, p1):
    """Time (in ms) to travel from `p0` to `p1`. This is a quasimetric."""

    x0, y0 = p0
    x1, y1 = p1

    x = x0
    ms = 0
    if y0 < y1:
        ms += downjump * (y1 - y0)
    elif y0 > y1:
        ms += pixel * abs(x0 - upward_teles)
        x = upward_teles
        ms += tele * (y0 - y1 - 1)  # `- 1` to account for teleporter delay
    ms += pixel * abs(x - x1)

    return ms


#### Precalculation of pairwise distances. ####
spawn_to_shelf = []
shelf_to_shelf = {}
shelf_to_hearth = []
for i, (_, x0, y0) in enumerate(bookshelves):
    coord0 = (x0, y0)

    spawn_to_shelf.append(d(spawnpoint, coord0))
    shelf_to_hearth.append(d(coord0, hearth))

    for j in range(i, len(bookshelves)):
        _, x1, y1 = bookshelves[j]
        coord1 = (x1, y1)

        shelf_to_shelf[i, j] = d(coord0, coord1)
        shelf_to_shelf[j, i] = d(coord1, coord0)

#### Calculating total times for all possible riddle lists. ####
min_time, max_time = (999_999, []), (0, [])
ts = []
for riddle_list in itertools.permutations(range(len(bookshelves)), 4):
    t = spawn_to_shelf[riddle_list[0]] + shelf_to_hearth[riddle_list[-1]]
    for i in range(len(riddle_list) - 1):
        t += shelf_to_shelf[riddle_list[i], riddle_list[i + 1]]
    ts.append(t)

    if t < min_time[0]:
        min_time = (t, [bookshelves[i][0] for i in riddle_list])
    if t > max_time[0]:
        max_time = (t, [bookshelves[i][0] for i in riddle_list])

#### Results! ####
ts.sort()
tsnp = np.array(ts)
print(f"min: {min_time}")
print(f"Q1: {np.quantile(tsnp, 1/4)}")
print(f"Q2: {np.median(tsnp)}")
print(f"expected: {np.mean(tsnp)}")
print(f"Q3: {np.quantile(tsnp, 3/4)}")
print(f"max: {max_time}")
with open("time_dump.bak", "w", encoding="UTF-8") as f:
    f.write(f'{{ "times": {ts} }}\n')
