import json
import matplotlib.pyplot as plt
import numpy as np

METADATA = {"Creator": None, "Date": None, "Format": None, "Type": None}

ts = None
with open("time_dump.bak", "r", encoding="UTF-8") as f:
    ts = np.array(json.load(f)["times"]) / 1000.0

fig, ax = plt.subplots(figsize=(12.0, 6.0), layout="constrained")

ax.set_xlabel("walking time (s)")
ax.set_ylabel("riddle lists")

ax.hist(ts, bins="fd")
ax.vlines(
    np.array([min(ts), np.median(ts), max(ts)]),
    0,
    8_000,
    colors="C1",
    linestyles="dashed",
)

ax.spines[["right", "top"]].set_visible(False)

plt.savefig("riddle-list-plot.svg", format="svg", metadata=METADATA)
