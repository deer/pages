"""Putting it all together, threegether, fourgether..."""

import itertools
import math
from util import BOX_POSITIONS, CHASE


JMS_ORDER = tuple(s - 1 for s in [1, 3, 6, 7, 4, 8, 2, 5, 9])
DMS_ORDER = tuple(s - 1 for s in [1, 3, 6, 7, 4, 2, 5, 8, 9])


def dms():
    """
    Returns zero-normalised combos, e.g. [4, 5, 6, 7, 8] instead of the in-game
    labelling [4, 2, 5, 8, 9].
    """

    n = 9
    for bs in CHASE:
        yield [i for i in range(n) if bs & (1 << i)]


def jms(n=9, k=5):
    """
    Returns zero-normalised combos, e.g. [0, 1, 2, 3, 4] instead of the in-game
    labelling [1, 3, 6, 7, 4].
    """

    # 1. [Initialise:]
    c = list(range(k))
    c.append(n)

    while True:
        # 2. [Cycle:]
        for j in range(c[1]):
            yield c[:k]

            if j < c[1] - 1:
                c[0] = (c[0] + 1) % c[1]

        # 3. [Lexicographic transition:]
        # Implementing the lexicographic part myself TO SHOW THAT I CAN!! (:<
        i = k - 1
        while c[i] + 1 == c[i + 1]:
            i -= 1
        if i < 1:
            return

        c[i] += 1
        i += 1
        while i < k:
            c[i] = c[i - 1] + 1
            i += 1


def gms(n=9, k=5):
    """
    Returns zero-normalised combos, e.g. [0, 1, 2, 3, 4] instead of the in-game
    labelling [1, 2, 3, 4, 5].
    """

    yield from itertools.combinations(range(n), k)


def path_hamming(combos):
    """
    Hamming distance of this path in combo string (**NOT** bit-string)
    presentation. Does not take into account distances between slots/labels in
    any way.
    """

    w = 0
    c_prev = None
    for c in combos:
        if c_prev is not None:
            w += sum(1 for s0, s1 in zip(c_prev, c) if s0 != s1)
        c_prev = c

    return w


def path_slot_dist(combos):
    """
    Distance traversed by this path in terms of slots, e.g.:

    - `123` -> `124` is a distance of 1,
    - `123` -> `125` is a distance of 2,
    - `123` -> `146` is a distance of 5,
    - ...etc....
    """

    d = 0
    c_prev = None
    for c in combos:
        if c_prev is not None:
            d += sum(abs(s0 - s1) for s0, s1 in zip(c_prev, c))
        c_prev = c

    return d


def dist_euclid(p0, p1):
    """
    Euclidean distance between points `p0` and `p1`.
    """
    dx, dy = p0[0] - p1[0], p0[1] - p1[1]

    return math.sqrt(dx * dx + dy * dy)


def path_pixel_dist(combos, order=(0, 1, 2, 3, 4, 5, 6, 7, 8)):
    """
    Total pixel distance travelled along this path by all PCs combined.

    `order` must be provided if you're not using the label order.
    """

    d = 0.0
    c_prev = None
    for c in combos:
        if c_prev is not None:
            d += sum(
                dist_euclid(BOX_POSITIONS[order[s0]], BOX_POSITIONS[order[s1]])
                for s0, s1 in zip(c_prev, c)
                if s0 != s1
            )
        c_prev = c

    return d


def main():
    """Print some shit."""

    for i, c in enumerate(dms()):
        print(f"{i + 1}. `{''.join(str(DMS_ORDER[s] + 1) for s in c)}`")
    print()
    print("==== Hamming distances ====")
    print(f"GMS: {path_hamming(gms())}")
    print(f"JMS: {path_hamming(jms())}")
    print(f"dMS: {path_hamming(dms())}")
    print()
    print("==== slotwise distances ====")
    print(f"GMS: {path_slot_dist(gms())}")
    print(f"JMS: {path_slot_dist(jms())}")
    print(f"dMS: {path_slot_dist(dms())}")
    print()
    print("==== pixel distances ====")
    print(f"GMS: {path_pixel_dist(gms())}")
    print(f"JMS: {path_pixel_dist(jms(), order=JMS_ORDER)}")
    print(f"dMS: {path_pixel_dist(dms(), order=DMS_ORDER)}")


if __name__ == "__main__":
    main()
