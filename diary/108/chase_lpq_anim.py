from util import CHASE

n, k = 9, 5
box_combs = [[i for i in range(n) if bs & (1 << i)] for bs in CHASE]
box_perm = [
    1,
    3,
    6,
    7,
    4,
    2,
    5,
    8,
    9,
]
box_perm = [n - 1 for n in box_perm]
# fmt: off
box_positions = [
    (0, 0), (1, 0),
    (0, 1), (1, 1), (2, 1),
    (0, 2), (1, 2), (2, 2), (3, 2),
]
# fmt: on
pc_colours = ["#9ea1d4", "#a8d1d1", "#f1f7b5", "#fd8a8a", "#eaecee"]
progress_colours = ["#36ae7c", "#f9d923", "#eb5353"]
sidelen, margin = 36, 12
aspect = 67 / 39  # The real in-game aspect ratio (ignoring box 9)
margin_big = round(aspect * (margin + sidelen) - sidelen)
progress_height = 3 * sidelen // 4
w = 4 * sidelen + (4 + 1) * margin_big
h = 3 * sidelen + (3 + 1) * margin + progress_height + margin
r = sidelen // 4
stroke = 2
pause = 4
tdur = len(CHASE) + pause
dur_s = 45
prelude = f"""<svg version="1.1" width="{w}" height="{h}" xmlns="http://www.w3.org/2000/svg">
  <rect width="100%" height="100%" fill="#111315" />
"""
postlude = """</svg>
"""

out = prelude

for i in range(n):
    box_pos, box_colour = box_positions[i], "#999c9f"
    x = margin_big + box_pos[0] * (sidelen + margin_big)
    y = margin + box_pos[1] * (sidelen + margin)
    out += f"""\n  <rect width="{sidelen + margin // 2}" height="{sidelen + margin // 2}" x="{x}" y="{y}" fill="transparent" stroke="{box_colour}" stroke-width="{stroke}" />"""
out += "\n"

for i in range(k):
    out += f"""\n  <rect width="{sidelen}" height="{sidelen}" x="{x}" y="{y}" rx="{r}" fill="{pc_colours[i]}">"""

    pos_durs = []
    for comb in box_combs:
        box = box_perm[comb[i]]
        box_pos = box_positions[box]
        pos = (
            margin_big + box_pos[0] * (sidelen + margin_big) + margin // 4,
            margin + box_pos[1] * (sidelen + margin) + margin // 4,
        )
        if len(pos_durs) == 0:
            pos_durs.append([pos, 1])
        elif pos_durs[-1][0] != pos:
            if pos_durs[-1][1] > 1:
                pos_durs[-1][1] -= 1
                pos_durs.append([pos_durs[-1][0], 1])
            pos_durs.append([pos, 1])
        else:
            pos_durs[-1][1] += 1
    pos_durs[-1][1] += pause

    durs = [pos_durs[0][1]]
    for [_, dur] in pos_durs[1:-1]:
        durs.append(durs[-1] + dur)
    key_times = "0;" + ";".join(str(dur / tdur) for dur in durs) + ";1"
    key_splines = ";".join("0.5 0.9 0.5 1" for _ in range(len(durs) + 1))
    for j, attr in enumerate(["x", "y"]):
        values = ";".join(str(pos[j]) for [pos, _] in pos_durs[:-1])
        l = pos_durs[-1][0][j]
        values += f";{l};{l}"
        out += f"""\n    <animate attributeName="{attr}" dur="{dur_s}s" calcMode="spline" repeatCount="indefinite" values="{values}" keyTimes="{key_times}" keySplines="{key_splines}" />"""

    out += "\n  </rect>\n"

out += f"""\n  <rect width="0" height="{progress_height}" x="{margin_big - (stroke // 2)}" y="{(3 + 1) * margin + 3 * sidelen}" fill="{progress_colours[0]}">"""
max_w = 4 * (margin_big + sidelen) - margin_big + margin // 2 + stroke
values = f"0;{max_w};{max_w}"
key_times = f"0;{(len(CHASE) - 1) / tdur};1"
out += f"""\n    <animate attributeName="width" dur="{dur_s}s" repeatCount="indefinite" values="{values}" keyTimes="{key_times}" />"""
values = ";".join(progress_colours) + ";" + progress_colours[-1]
key_times = (
    ";".join(
        str(i * (len(CHASE) - 1) / tdur / (len(progress_colours) - 1))
        for i in range(len(progress_colours))
    )
    + ";1"
)
out += f"""\n    <animate attributeName="fill" dur="{dur_s}s" repeatCount="indefinite" values="{values}" keyTimes="{key_times}" />"""
out += "\n  </rect>\n"

out += postlude

with open("chase_lpq_anim.svg", "w", encoding="UTF-8") as f:
    f.write(out)
