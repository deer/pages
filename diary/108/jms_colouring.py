n, k = 9, 5
box_perm = [
    1,
    3,
    6,
    7,
    4,
    8,
    2,
    5,
    9,
]
box_perm = [n - 1 for n in box_perm]
box_order = [0] * n
for i, b in enumerate(box_perm):
    box_order[b] = i
yellow, red, blue, green = "#f7f47b", "#f47c7c", "#70a1d7", "#81de63"
# fmt: off
box_positions = [
    (0, 0), (1, 0),
    (0, 1), (1, 1), (2, 1),
    (0, 2), (1, 2), (2, 2), (3, 2),
]
box_colours = [
    yellow, green,
    yellow, blue,  green,
    yellow, red,   blue,  green,
]
# fmt: on
sidelen, margin = 36, 12
aspect = 67 / 39  # The real in-game aspect ratio (ignoring box 9)
margin_big = round(aspect * (margin + sidelen) - sidelen)
w = 4 * sidelen + (4 + 1) * margin_big
h = 3 * sidelen + (3 + 1) * margin
stroke = 4
font_stack = "-apple-system, BlinkMacSystemFont, 'Avenir Next', Avenir, 'Segoe UI', 'Helvetica Neue', Helvetica, Cantarell, Ubuntu, Roboto, Noto, Arial, sans-serif"
prelude = f"""<svg version="1.1" width="{w}" height="{h}" xmlns="http://www.w3.org/2000/svg">
  <rect width="100%" height="100%" fill="#111315" />
"""
postlude = """</svg>
"""

out = prelude

for i in range(n):
    box_pos, box_colour = box_positions[i], box_colours[i]
    x = margin_big + box_pos[0] * (sidelen + margin_big)
    y = margin + box_pos[1] * (sidelen + margin)
    out += f"""\n  <rect width="{sidelen}" height="{sidelen}" x="{x}" y="{y}" fill="transparent" stroke="{box_colour}" stroke-width="{stroke}" />"""
    text_x, text_y = x + 5 * sidelen // 16, y + 3 * sidelen // 4
    out += f"""\n  <text x="{text_x}" y="{text_y}" fill="{box_colour}" font-size="{3 * sidelen // 4}" font-family="{font_stack}">{box_order[i] + 1}</text>\n"""

out += postlude

with open("jms_colouring.svg", "w", encoding="UTF-8") as f:
    f.write(out)
