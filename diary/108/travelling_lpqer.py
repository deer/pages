"""
Solves an open TSP, starting at *any* vertex (whichever yields the best
result), and prints the best result.
"""

from python_tsp.distances import euclidean_distance_matrix
from python_tsp.exact import solve_tsp_dynamic_programming
from util import BOX_POSITIONS

box_positions = BOX_POSITIONS[:]
best_perm, best_dist = [], float("+inf")
for shift in range(len(box_positions)):
    dist_matrix = euclidean_distance_matrix(box_positions)
    dist_matrix[:, 0] = 0
    perm, dist = solve_tsp_dynamic_programming(dist_matrix)

    if dist < best_dist:
        best_perm, best_dist = [
            (n + shift) % len(box_positions) + 1 for n in perm
        ], dist

    p = box_positions.pop(0)
    box_positions.append(p)

print(best_perm)
print(best_dist)
