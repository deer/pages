use bit_vec::BitVec;

pub struct AlgorithmC {
    a: BitVec, // combo
    w: BitVec, // auxiliary table
    n: usize,  // n = t + s
    r: usize,  // auxiliary
    first: bool,
}

impl AlgorithmC {
    pub fn new(n: usize, t: usize) -> Self {
        let s = n - t;
        // C1. [Initialize.]
        let a = BitVec::from_fn(n, |j| s <= j);
        let w = BitVec::from_elem(n + 1, true);
        let r = if s > 0 { s } else { t };

        Self {
            a,
            w,
            n,
            r,
            first: true,
        }
    }

    pub fn next(&mut self) -> Option<&BitVec> {
        if self.first {
            self.first = false;

            // C2. [Visit.]
            return Some(&self.a);
        }

        // C3. [Find j and branch.]
        let mut j = self.r;
        while !self.w[j] {
            self.w.set(j, true);
            j += 1;
        }
        if j == self.n {
            return None;
        }
        self.w.set(j, false);

        if self.a[j] {
            if j & 1 == 1 || self.a[j - 2] {
                // C4. [Move right one.]
                self.a.set(j - 1, true);
                self.a.set(j, false);
                if self.r == j && j > 1 {
                    self.r = j - 1;
                } else if self.r == j - 1 {
                    self.r = j;
                }
            } else {
                // C5. [Move right two.]
                self.a.set(j - 2, true);
                self.a.set(j, false);
                if self.r == j {
                    self.r = (j - 2).max(1);
                } else if self.r == j - 2 {
                    self.r = j - 1;
                }
            }
        } else {
            if j & 1 == 0 || self.a[j - 1] {
                // C6. [Move left one.]
                self.a.set(j, true);
                self.a.set(j - 1, false);
                if self.r == j && j > 1 {
                    self.r = j - 1;
                } else if self.r == j - 1 {
                    self.r = j;
                }
            } else {
                // C7. [Move left two.]
                self.a.set(j, true);
                self.a.set(j - 2, false);
                if self.r == j - 2 {
                    self.r = j;
                } else if self.r == j - 1 {
                    self.r = j - 2;
                }
            }
        }

        // C2. [Visit.]
        Some(&self.a)
    }
}
