mod algorithm_c;

use algorithm_c::AlgorithmC;
use bit_vec::BitVec;

fn main() -> Result<(), &'static str> {
    let mut ac = AlgorithmC::new(6, 3);
    while let Some(combo) = ac.next() {
        println!("{combo:?}");
    }
    println!();
    let mut ac = AlgorithmC::new(9, 5);
    while let Some(combo) = ac.next() {
        println!("{combo:?}");
    }
    println!();
    let mut ac = AlgorithmC::new(9, 5);
    let mut prev_combo = ac.next().unwrap().clone();
    let mut large_transitions = 0;
    while let Some(combo) = ac.next() {
        if transition_size(&prev_combo, combo) > 1 {
            large_transitions += 1;
        }
        prev_combo = combo.clone();
    }
    println!("{large_transitions}");

    println!("\n... Sanity check that takes a while to run :D ...");
    let mut ac = AlgorithmC::new(40, 11);
    let mut prev_combo = ac.next().unwrap().clone();
    while let Some(combo) = ac.next() {
        sanity_check(&prev_combo, combo)?;
        prev_combo = combo.clone();
    }

    Ok(())
}

fn sanity_check(x: &BitVec, y: &BitVec) -> Result<(), &'static str> {
    if x.len() != y.len() {
        return Err("mismatched lengths");
    }

    if x.iter().filter(|b| *b).count() != y.iter().filter(|b| *b).count() {
        return Err("mismatched weights");
    }

    let mut xor = x.clone();
    xor.xor(y);
    if xor.iter().filter(|b| *b).count() != 2 {
        return Err("x - y != 2");
    }

    let mut differ =
        xor.iter()
            .enumerate()
            .filter_map(|(i, b)| if b { Some(i) } else { None });
    let i = differ.next();
    let j = differ.next();
    match (i, j) {
        (Some(i), Some(j)) => {
            if j - i > 2 {
                return Err("not near-perfect");
            }
        }
        _ => return Err("wat."),
    }

    Ok(())
}

fn transition_size(x: &BitVec, y: &BitVec) -> usize {
    let mut xor = x.clone();
    xor.xor(y);
    let mut differ =
        xor.iter()
            .enumerate()
            .filter_map(|(i, b)| if b { Some(i) } else { None });
    let i = differ.next();
    let j = differ.next();
    match (i, j) {
        (Some(i), Some(j)) => j - i,
        _ => unreachable!(),
    }
}
