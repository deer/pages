#!/usr/bin/env bash

set -e

if [[ -z $1 ]]; then
    echo 'Expected exactly one argument: the path to the deer_website repository' >&2
    exit 1
fi

rsync -rpth --progress "$1"/public/ .
